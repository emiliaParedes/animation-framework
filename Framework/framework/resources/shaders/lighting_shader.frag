#version 400

out vec4 out_color;

in vec4 vtx_color;
in vec2 vtx_uv;
in vec3 vtx_normal;
in vec3 vtx_pos;

uniform sampler2D texture_data_0;
uniform float use_vertex_color;

//specular lighting
uniform vec3 camera_pos;
uniform vec3 light_color;
uniform vec3 light_position;
uniform float light_intensity;
uniform vec3 diffuse_color;
//uniform float globalAmbient;

void main(){	

	// ambient
	//material.ambient = vec3(0.1);	///---------
	
	vec4 diff_color = texture(texture_data_0, vtx_uv);
	vec4 frag_color = diff_color;
	if(use_vertex_color > 0.5)
		frag_color = vec4(diffuse_color, 1.0);
	
	
	float ambient_strength = 0.1;
    vec3 ambient = ambient_strength * light_color;
  	
    // diffuse 
    vec3 lightDir = normalize(light_position - vtx_pos);
    float diff = max(dot(vtx_normal, lightDir), 0.10);
    vec3 diffuse = diff * light_color;
    
    // specular
    
	//vec3 viewDir = normalize(camera_pos - vtx_pos);
    //vec3 reflectDir = reflect(-lightDir, vtx_normal);  
    //float spec = pow(max(dot(viewDir, reflectDir), 0.10), 32);
    //vec3 specular = material.specular * spec * light[0].color; 
    
	vec3 result = (ambient + diffuse) * frag_color.rgb * light_intensity;
    out_color = vec4(result, diff_color.a);
	
//	if(use_vertex_color > 0.5)
//		out_color = vec4((ambient + diffuse) * diffuse_color, 1.0) * light_intensity;
//		//out_color = vec4((ambient + diffuse + specular) * material.diffuse, 1.0);
//    else 
//		out_color = vec4(ambient + diffuse*  * light_intensity, 1.0f);
		//out_color = vec4(ambient + diffuse + specular, 1.0) * texture(texture_data_0, vtx_uv).rgba;
	
	if(out_color.a < 0.01)
        discard;
	//out_color = texture(texture_data_0, vtx_uv).rgba;
}






