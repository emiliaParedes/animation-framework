#version 400


out vec4 out_color;

in vec2 vtx_uv;
in float use_color;
in vec4 frag_color;

uniform sampler2D texture_data_0;
uniform vec3 diffuse;

void main(){	
	vec4 final_color = vec4(diffuse, 1.0);
	
	if(use_color < 0.5)
		final_color = texture(texture_data_0, vtx_uv).rgba;
		
	if(final_color.a <= 0.0)
		discard;
		
	out_color = final_color;
}

