#version 400

layout (location = 0) out vec3 g_position;
layout (location = 1) out vec3 g_normal;
layout (location = 2) out vec4 g_albedo;

in vec4 vtx_pos;
in vec2 vtx_uv;
in vec3 vtx_normal;

uniform sampler2D texture_diffuse;
uniform sampler2D texture_specular;

void main(){
	g_position = vtx_pos.xyz;
	
	g_normal = normalize(vtx_normal);
	
	g_albedo.rgb = texture(texture_diffuse, vtx_uv).rgb;
	
	g_albedo.a = texture(texture_specular, vtx_uv).r;
}