#include "animation.h"
#include "scene.h"
#include "object.h"

#include <iostream>

animation::~animation() {
	for (channel* c : m_channels)
		delete c;
}

void animation::initialize(scene* sc) {

	for (channel* c : m_channels) {
		c->initialize();
		m_object_targets.push_back(sc->get_object(c->m_target));
	}
}

float animation::get_time_from_ticks(float t) {
	return m_ticks_per_second == 0.0f ? t : t * m_ticks_per_second;
}

void animation::update(float time) {
	//do something here 
	float t = get_time_from_ticks(time);
	animation::poses poses = produce_poses(t);
	//apply poses 
	for (object* o : m_object_targets) {
		auto search = poses.find(o->m_name);
		if (search != poses.end())
			o->m_local = search->second;
	}
}

animation::poses animation::produce_poses(float curr_time) {
	poses new_poses{};
	for (channel* c : m_channels) {
		std::string name = c->m_target;
		transform trans = c->get_transform(curr_time);
		new_poses.insert(std::make_pair(name, trans));
	}
	return new_poses;
}
