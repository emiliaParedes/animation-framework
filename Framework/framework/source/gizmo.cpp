/*!
 * \file  gizmo.cpp
 * \brief This is a file that contains definition of functions for class gizmo
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "gizmo.h"
#include "camera.h"
#include "graphics_manager.h"
#include "geometry.h"
#include "scene.h"
#include "object.h"
#include "animator.h"

#include <iostream>
#include <glm\gtc\type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include "../dependencies/src/imgui/imgui.h"

 /*!
  * \brief constructor
  */
gizmo::gizmo()
	: m_mouse(0.0f),
	m_hover(nullptr),
	m_locked(nullptr),
	m_operation(ImGuizmo::TRANSLATE)
{}

/*!
 * \brief destructor
 */
gizmo::~gizmo() {
}

/*!
 * \brief ImGuizmo init
 */
void gizmo::initialize() {
	ImGuizmo::SetOrthographic(false);
	//true by default
	ImGuizmo::Enable(true);
}

/*!
 * \brief checks the ray on obj to check if choosing another
 */
void gizmo::update(const float& dt) {
	ImGuizmo::BeginFrame();
	//camera position 
	camera* cam = g_gfx_man()->get_camera();
	ray c_ray{cam->m_position, m_mouse};
	//check with objects
	std::vector<object*>& objs = g_gfx_man()->get_scene()->m_objects;
	float min_dist = c_max_float;
	
	object* out = nullptr;
	for (object* obj : objs)
		get_intersection(c_ray, obj, min_dist, out);

	if (out != nullptr) {
		//render hover
		m_hover = out;
	}	
	else {
		m_hover = nullptr;
	}

}

/*!
 * \brief renders the gizmo, hovered and locked bv and if locked, the other window
 */
void gizmo::render(const glm::mat4& view, const glm::mat4 proj) {
	if (m_hover != nullptr && m_hover != m_locked) {
		//render hover color
		g_gfx_man()->render_cube(m_hover->get_world_bv(), c_hovered);
	}
	if (m_locked != nullptr) {
		//render locked and transform 
		g_gfx_man()->render_cube(m_locked->get_world_bv(), c_locked);
		//transform obj
		edit_object(view, proj);
		//imgui transform of obj
		edit_object_ui();
	}
	//point
	scene* sc = g_gfx_man()->get_scene();
	if (sc->m_show_curves && (sc->m_selected_point.first != -1 || sc->m_selected_point.second != -1)) {
		edit_curve(view, proj);
		edit_curve_ui();
	}
}

/*!
 * \brief locks the hovered object
 */
void gizmo::lock() {
	if (ImGuizmo::IsOver())
		return;
	if (m_hover != nullptr && !ImGuizmo::IsUsing())
		m_locked = m_hover;
	else
		m_locked = nullptr;

	scene* sc = g_gfx_man()->get_scene();
	//if (sc->m_hovered_point.first != -1)
	sc->m_selected_point.first = sc->m_hovered_point.first;
	//if (sc->m_hovered_point.second != -1)
	sc->m_selected_point.second = sc->m_hovered_point.second;
}

/*!
 * \brief if there is an item locked
 */
bool gizmo::in_use() {
	if (m_locked != nullptr)
		return true;
	return false;
}

/*!
 * \brief updates the position of the mouse to get the ray check
 */
void gizmo::update_mouse_pos(const double x, const double y) {
	camera* cam = g_gfx_man()->get_camera();
	glm::mat4 inv = cam->get_proj_mat();
	inv = glm::inverse(inv);

	glm::vec4 mouse_pos(x, y, 0.0, 1.0);
	//normalize
	mouse_pos.x = (2.0f * mouse_pos.x) / g_gfx_man()->get_width() - 1.0f;
	mouse_pos.y = 1.0f - (2.0f * mouse_pos.y) / g_gfx_man()->get_height();

	glm::vec4 ray_eye = inv * mouse_pos;
	ray_eye.z = -1.0f;
	ray_eye.w = 0.0f;

	glm::mat4 view = cam->get_view_mat();
	glm::vec3 ray_word = glm::vec3(inverse(view) * ray_eye);
	//normalize 
	ray_word = glm::normalize(ray_word);

	m_mouse = ray_word;
}

/*!
 * \brief sets trans, rot or sca 
 */
void gizmo::set_operation(const int& op) {
	if (in_use()) {
		if (op == 0)
			m_operation = ImGuizmo::TRANSLATE;
		else if (op == 1)
			m_operation = ImGuizmo::SCALE;
		else
			m_operation = ImGuizmo::ROTATE;
	}
}

/*!
 * \brief resets vlaues
 */
void gizmo::reset() {
	m_locked = nullptr;
	m_hover = nullptr;
	m_operation = ImGuizmo::TRANSLATE;
}

void gizmo::get_curve_intersection(scene* sc) {
	camera* cam = g_gfx_man()->get_camera();
	ray c_ray{ cam->m_position, m_mouse };
	float min_dist = c_max_float;
	bool found = false;
	picewise_curve pw = sc->m_curves[sc->m_curve_show];
	for (int i = 0; i < pw.m_points.size(); i++) {
		curve_point cp = pw.m_points[i];
		if (i != 0)
			if (pw.m_points[i - 1].m_point == cp.m_point)
				continue;
		if (get_intersection(c_ray, cp.m_point, sc->m_point_size, min_dist)) {
			sc->m_hovered_point.first = i;
			found = true;
		}
	}
	if (!found)
		sc->m_hovered_point.first = -1;
	found = false;
	for (int i = 0; i < pw.m_support.size(); i++) {
		support_point sp = pw.m_support[i];
		if (get_intersection(c_ray, sp.m_control_point, sc->m_point_size, min_dist)) {
			sc->m_hovered_point.second = i;
			found = true;
		}
	}
	if (!found)
		sc->m_hovered_point.second = -1 ;
}

/*!
 * \brief editing of object in world 
 */
void gizmo::edit_object(const glm::mat4& view, const glm::mat4& proj) const {
	//send the matrix with the world position and scale of the object with no rotation
	glm::mat4 mat = glm::translate(m_locked->m_world.m_position) * glm::scale(m_locked->m_world.m_scale);

	auto look = glm::value_ptr(view);
	auto pro = glm::value_ptr(proj);
	glm::mat4 id = glm::mat4(1);
	float* identity = glm::value_ptr(id);

	ImGuiIO& io = ImGui::GetIO();
	ImGuizmo::SetRect(1, 1, io.DisplaySize.x, io.DisplaySize.y);

	ImGuizmo::Manipulate(
		look,
		pro,
		m_operation,
		ImGuizmo::WORLD,
		&mat[0][0],
		identity,
		NULL,
		NULL,
		NULL);

	glm::vec3 pos{};
	glm::vec3 rot{};
	glm::vec3 sca{};

	ImGuizmo::DecomposeMatrixToComponents(&mat[0][0], glm::value_ptr(pos), glm::value_ptr(rot), glm::value_ptr(sca));
	
	switch (m_operation) {
		case ImGuizmo::TRANSLATE: {
			//just set it 
			m_locked->m_world.m_position = pos;
			break;
		}
		case ImGuizmo::ROTATE: {
			//get rads instead of deg
			rot = glm::radians(rot);
			//create rotation matrix
			glm::mat4 rot_mat = glm::rotate(rot.x, glm::vec3(1.0f, 0.0f, 0.0f))*
								glm::rotate(rot.y, glm::vec3(0.0f, 1.0f, 0.0f)) *
								glm::rotate(rot.z, glm::vec3(0.0f, 0.0f, 1.0f));
			//get the new quaternion by multiplying the new rotation and the last world quaternion
			m_locked->m_world.m_quaternion = glm::normalize(glm::toQuat(rot_mat) * m_locked->m_world.m_quaternion);
			break;
		}
		case ImGuizmo::SCALE: {
			//just set
			m_locked->m_world.m_scale = sca;
		}
	}
	m_locked->update_local_from_world();
}

/*!
 * \brief renders ui inspector
 */
void gizmo::edit_object_ui() {
	//sanity check 
	if (m_locked == nullptr)
		return ;
	ImGui::Begin("Object Info");

	ImGui::Text("Name: ");
	ImGui::SameLine();
	ImGui::Text(m_locked->m_name.c_str());

	ImGui::NewLine();
	if (ImGui::Button("Delete Object")) {
		delete m_locked;
		m_locked = nullptr;
		ImGui::End();
		return;
	}
	ImGui::NewLine();
	
	ImGui::Text("Parent: ");
	ImGui::SameLine();
	if (m_locked->m_parent == nullptr)
		ImGui::Text("Scene");
	else 
		ImGui::Text(m_locked->m_parent->m_name.c_str());

	transform local = m_locked->m_local;
	transform world = m_locked->m_world;
	
	ImGui::NewLine();
	ImGui::Checkbox("Visible", &m_locked->m_visible);
	ImGui::Checkbox("Render Skelleton", &m_locked->m_render_skelly);
	if (m_locked->m_animator && ImGui::CollapsingHeader("Animation") ){
		ImGui::Checkbox("Pause", &m_locked->m_animator->m_paused);
		ImGui::Checkbox("Loop", &m_locked->m_animator->m_loop);
		if (ImGui::Button("Reset animation"))
			m_locked->m_animator->reset_animation();
	}

	ImGui::NewLine();

	if (ImGui::CollapsingHeader("Transform")) {
		ImGui::Text("Local Position");
		if (ImGui::DragFloat3("", &(local.m_position[0])))
			m_locked->update_local(local);

		ImGui::Text("Local Scale");
		if (ImGui::DragFloat3(" ", &(local.m_scale[0])))
			m_locked->update_local(local);

		ImGui::Text("Local Rotation");
		if (ImGui::DragFloat4("  ", &(local.m_quaternion[0])))
			m_locked->update_local(local);

		ImGui::NewLine();

		ImGui::Text("World Position. Read Only");
		ImGui::DragFloat3("   ", &(world.m_position[0]));

		ImGui::Text("World Scale");
		ImGui::DragFloat3("    ", &(world.m_scale[0]));

		ImGui::Text("World Rotation");
		ImGui::DragFloat4("     ", &(world.m_quaternion[0]));
	}
	ImGui::End();
}

void gizmo::edit_curve(const glm::mat4& view, const glm::mat4& proj) const {
	scene* sc = g_gfx_man()->get_scene();
	//send the matrix with the world position and scale of the object with no rotation
	glm::mat4 mat{};
	if (sc->m_selected_point.first != -1) 
		mat = glm::translate(sc->m_curves[sc->m_curve_show].m_points[sc->m_selected_point.first].m_point) * glm::scale(sc->m_point_size);
	else if(sc->m_selected_point.second != -1)
		mat = glm::translate(sc->m_curves[sc->m_curve_show].m_support[sc->m_selected_point.second].m_control_point) * glm::scale(sc->m_point_size);
	
	
	auto look = glm::value_ptr(view);
	auto pro = glm::value_ptr(proj);
	glm::mat4 id = glm::mat4(1);
	float* identity = glm::value_ptr(id);

	ImGuiIO& io = ImGui::GetIO();
	ImGuizmo::SetRect(1, 1, io.DisplaySize.x, io.DisplaySize.y);

	ImGuizmo::Manipulate(
		look,
		pro,
		m_operation,
		ImGuizmo::WORLD,
		&mat[0][0],
		identity,
		NULL,
		NULL,
		NULL);

	glm::vec3 pos{};
	glm::vec3 rot{};
	glm::vec3 sca{};

	ImGuizmo::DecomposeMatrixToComponents(&mat[0][0], glm::value_ptr(pos), glm::value_ptr(rot), glm::value_ptr(sca));

	switch (m_operation) {
		case ImGuizmo::TRANSLATE: {
			//just set it 
			picewise_curve& pw = sc->m_curves[sc->m_curve_show];
			int pos_idx = sc->m_selected_point.first;
			if (pos_idx != -1) {
				if (pos_idx < sc->m_curves[sc->m_curve_show].m_points.size() - 1) {
					if (pw.m_points[pos_idx + 1].m_distance == pw.m_points[pos_idx].m_distance) {
						pw.m_points[pos_idx + 1].m_point = pos;
						std::cout << "lol0" << std::endl;
					}
				}
				pw.m_points[pos_idx].m_point = pos;
			}
			else if (sc->m_selected_point.second != -1)
				sc->m_curves[sc->m_curve_show].m_support[sc->m_selected_point.second].m_control_point = pos;
			break;
		}
		case ImGuizmo::SCALE: {
			//just set
			sc->m_point_size = sca;
			break;
		}
	}
}

void gizmo::edit_curve_ui() {
	ImGui::Begin("Curve Info");
	scene* sc = g_gfx_man()->get_scene();
	picewise_curve& pw = sc->m_curves[sc->m_curve_show];
	//point
	if (sc->m_selected_point.first != -1) {
		int p = sc->m_selected_point.first;
		curve_point& cp = pw.m_points[p];
		glm::vec3 point_ = cp.m_point;
		float dist_ = cp.m_distance;
		ImGui::Text("Point");
		ImGui::Text("Time/Distance");
		ImGui::DragFloat(" ", &(dist_));
		ImGui::Text("Position");
		ImGui::DragFloat3("", &(point_[0]));
		if (sc->m_curve_show != 0 && sc->m_curve_show != 3) {
			if (p >= 1) {
				glm::vec3 temp = pw.m_support[p].m_control_point;
					ImGui::Text("Tangent/Control Point 0");
					ImGui::DragFloat3("0", &(temp[0]));
					pw.m_support[p].m_control_point = temp;
			}
			if (sc->m_selected_point.first < pw.m_points.size() - 1) {
				glm::vec3 cp = pw.m_support[sc->m_selected_point.first + 1].m_control_point;
				ImGui::Text("Tangent/Control Point 1");
				ImGui::DragFloat3("1", &(cp[0]));
				pw.m_support[sc->m_selected_point.first + 1].m_control_point = cp;
			}
			if (p < pw.m_points.size() - 1) {
				if (cp.m_point == pw.m_points[p + 1].m_point) {
					pw.m_points[p + 1].m_point = point_;
					pw.m_points[p + 1].m_distance = dist_;
				}
				pw.m_points[p + 1].m_distance = dist_;
			}
		}
		cp.m_point = point_;
		cp.m_distance = dist_;
		

	}
	//tangent
	else if (sc->m_selected_point.second != -1) {
		ImGui::Text("Tangent");
		ImGui::Text("Position");
		ImGui::DragFloat3("", &(pw.m_support[sc->m_selected_point.second].m_control_point[0]));
	}

	ImGui::End();

}
