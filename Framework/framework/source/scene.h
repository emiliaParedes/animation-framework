/*!
 * \file  scene.hs
 * \brief This is a file that contains declaration of class scene
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once
#include <vector>
#include <string>

#include "curves.h"

class object;
class light;
class resource_manager;
class camera;
class importer;
class animator;

class scene {
	public:
		scene();
		~scene();
		//void load_scene(const std::string& path);
		void load_scene(const std::vector<std::string>& paths);
		void load_scene(const std::string& path);
		void load_curves();
		void unload_scene();
		void render();
		void update(const float& dt);
		object* get_object(std::string name);
		bool delete_object(std::string name);
		void add_light();
		void ui_render();
		void ui_node(const object* node) const;
		void ui_render_nodes_rec(object* node, const object* curr_obj);
		bool ui_change_parent(object*& out_obj, const object* curr_obj);
		std::vector<picewise_curve> m_curves;
		camera* m_camera;
		std::vector<object*> m_objects;
		std::vector<light*> m_lights;
		importer* m_importer = nullptr;
		object* m_parent_change = nullptr;
		float m_joint_size = 0.01f;
		unsigned m_curve_show = 3;
		bool m_show_curves = false;
		glm::vec3 m_point_size{1.0f};
		std::pair<int, int> m_selected_point = {-1, -1};
		std::pair<int, int> m_hovered_point = {-1, -1};
		float m_curve_time = 0.0f;
		unsigned m_presicion = 50;
		glm::vec4 m_curve_obj_color{1.0f};

	private: 
		bool del_obj(object* curr, std::string name);
};