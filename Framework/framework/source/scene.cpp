/*!
 * \file  scene.cpp
 * \brief This is a file that contains definition of functions for class scene
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "scene.h"

//libs
#include <assimp/cimport.h>
#include <assimp/postprocess.h>	//not sure about this one 
#include <assimp/scene.h>
#include <vector>
#include <glm/gtc/random.hpp>

//imgui 
#include "..\dependencies\src\imgui\imgui.h"
#include "..\dependencies\src\imgui\imgui_impl_glfw.h"
#include "..\dependencies\src\imgui\imgui_impl_opengl3.h"

//graphics
#include "shader.h"
#include "resources.h"
#include "mesh.h"
#include "graphics_manager.h"
#include "scene_loader.h"
#include "camera.h"
#include "importer.h"
#include "object.h"
#include "light.h"
#include "gizmo.h"
#include "animator.h"

 /*!
  * \brief constructor
  */
scene::scene()
	: m_camera{ nullptr },
	m_objects{}, 
	m_lights{},
	m_importer{nullptr}
{
	scene* curr_scene = g_gfx_man()->get_scene();
	if (curr_scene != nullptr)
		curr_scene->unload_scene();
	g_gfx_man()->set_scene(this);
	if (m_camera == nullptr)
		m_camera = new camera();

	if (m_importer == nullptr)
		m_importer = new importer{};

	add_light();

	load_curves();
	
}
/*!
 * \brief destructor
 */
scene::~scene() {
	unload_scene();
	if (m_importer != nullptr)
		delete m_importer;
	m_importer = nullptr;
	if (m_camera != nullptr)
		delete m_camera;
	m_camera = nullptr;
}

/*void scene::load_scene(const std::string& path) {
	

	std::vector<std::string> scenes{};
	//scenes.push_back("assets/sponza/Sponza.gltf");
	//scenes.push_back("assets/box/Box.gltf");
	scenes.push_back(path);
	//scenes.push_back("data/meshes/sponza.obj");
	//scenes.push_back("data/meshes/suzanne.obj");
	m_objects.push_back(m_importer->import_file(scenes));

	for (object* obj : m_objects)
		obj->initialize();
	//scene_loader(path, m_camera, m_objects);

}*/

/*!
 * \brief loads the scenes from file paths
 */
void scene::load_scene(const std::vector<std::string>& paths) {
	for(const std::string& s : paths)
		m_objects.push_back(m_importer->import_file(s));

	for (object* obj : m_objects)
		obj->initialize();	
}

/*!
 * \brief same
 */
void scene::load_scene(const std::string& path) {
	m_objects.push_back(m_importer->import_file( path ));
	m_objects.back()->initialize();
}

void scene::load_curves(){
	m_curves.clear();
	m_curves.push_back(picewise_curve(picewise_curve::curve_type::pwc_linear));
	m_curves.push_back(picewise_curve(picewise_curve::curve_type::pwc_bezier));
	m_curves.push_back(picewise_curve(picewise_curve::curve_type::pwc_hermite));
	m_curves.push_back(picewise_curve(picewise_curve::curve_type::pwc_catmull));
}

/*!
 * \brief unload all assets and objects from scene
 */
void scene::unload_scene() {
	for (object* obj : m_objects)
		delete obj;
	m_objects.clear();
	//for (light* lig : m_lights)
	//	delete lig;
	//m_lights.clear();
	m_parent_change = nullptr;
}

void scene::render() {
	if (m_show_curves) {
		m_curves[m_curve_show].render(m_presicion, m_hovered_point, m_selected_point);
		glm::vec3 cube_pos = m_curves[m_curve_show].sample(m_curve_time);
		g_gfx_man()->render_cube(cube_pos, m_point_size * 2.0f, m_curve_obj_color);
	}
}

/*!
  * \brief updates the scene and objects
  */
void scene::update(const float& dt) {
	for (object* node : m_objects)
		node->update(dt);
}

object* get_obj(object* curr, std::string name) {
	if (curr->m_name == name)
		return curr;
	for (object* c : curr->m_children) {
		object* obj = get_obj(c, name);
		if (obj != nullptr)
			return obj;
	}
	return nullptr;
}

object* scene::get_object(std::string name) {
	for (object*& obj : m_objects) {
		if (obj->m_name == name)
			return obj;
		object* o =  get_obj(obj, name);
		if (o != nullptr)
			return o;
	}
	return nullptr;
}

bool scene::del_obj(object* curr, std::string name) {
	if (curr->m_name == name) {
		auto o = std::find(m_objects.begin(), m_objects.end(), curr);
		delete curr;
		m_objects.erase(o);
		return true;
	}
	for (object* c : curr->m_children) {
		if(del_obj(c, name))
			return true;
	}
	return false;
}

bool scene::delete_object(std::string name) {
	for (int i = 0; i < m_objects.size(); i++) {
		object* obj = m_objects[i];
		if (obj->m_name == name) {
			auto o = std::find(m_objects.begin(), m_objects.end(), obj);
			delete obj;
			m_objects.erase(o);
			return true;
		}
		if (del_obj(obj, name))
			return true;
	}
}

/*!
 * \brief adds a reg light
 */
void scene::add_light() {

	light* new_light = new light();
	//new_light->m_color = glm::sphericalRand(1.0f);
	//new_light->m_transform.m_position = glm::vec3(0.0f);// glm::sphericalRand(30.0f);// +glm::vec3(0.0f, 0.0f, 0.0f);
	m_lights.push_back(new_light);
}

/*!
 * \brief renders the ui of the scene
 */
void scene::ui_render() {

	//all curve information 
	if (ImGui::CollapsingHeader("Spooky Curves")) {
		ImGui::Checkbox("Show curves", &m_show_curves);
		std::vector<std::string> curves {"Linear", "Bezier", "Hermite", "Catmul"};
		std::string curr = curves[m_curve_show];
		if (ImGui::BeginCombo("Curve To Render:", curr.data())) {
			for (unsigned i = 0; i < 4; i++) {
				bool b = i == m_curve_show;
				if (ImGui::Selectable(curves[i].data(), b))
					m_curve_show = i;
			}
			ImGui::EndCombo();
		}
		ImGui::Text("Point size");
		ImGui::DragFloat3(" ", &m_point_size[0]);
		ImGui::DragFloat("Curve Time", &m_curve_time, 0.01f, 0.0f, 1.0f);
		int p = static_cast<int>(m_presicion);
		ImGui::DragInt("Samples", &p, 1, 0);
		m_presicion = static_cast<unsigned>(glm::clamp(p, 1, 100000));
		if (m_curve_show == 0 || m_curve_show == 3) {
			if (ImGui::Button("Add point"))
				m_curves[m_curve_show].m_points.push_back({});
		}
		auto& pw = m_curves[m_curve_show];
		if (ImGui::CollapsingHeader("Points")) {
			for (int i = 0; i < pw.m_points.size(); i++) {
				auto& p = pw.m_points[i];
				ImGui::DragFloat3(std::to_string(i).data(), &p.m_point[0]);
			}
		}
		if (pw.m_support.size() > 0 && ImGui::CollapsingHeader("Tangents")) {
			for (int i = 0; i < pw.m_support.size(); i++) {
				auto& p = pw.m_support[i];
				ImGui::DragFloat3((std::to_string(i) + " ").data(), &p.m_control_point[0]);
			}
		}
	}
	ImGui::NewLine();

	if (ImGui::CollapsingHeader("Lights")){
		/*ALL LIGHTS HERE */
		ImGui::Text("Position");
		ImGui::DragFloat3("", &m_lights[0]->m_transform.m_position[0]);
		ImGui::Text("Color");
		ImGui::DragFloat3(" ", &m_lights[0]->m_color[0]);
		ImGui::Text("Intensity");
		ImGui::DragFloat("  ", &m_lights[0]->m_constant, 0.001f, 0.0f, 1.0f);
	}
	ImGui::NewLine();

	if (ImGui::CollapsingHeader("Objects:")) {
		for (object* obj : m_objects)
			ui_node(obj);
	}
}

/*!
 * \brief recursive for ui
 */
void scene::ui_node(const object* node) const {
	if (ImGui::TreeNode(node->m_name.c_str())) {
		if (ImGui::IsMouseClicked(1)){
			g_gfx_man()->get_gizmo()->lock(const_cast<object*>(node));
		}
		for (object* child: node->m_children)
			ui_node(child);
		ImGui::TreePop();
	}
}

/*!
 * \brief recursive for ui
 */
void scene::ui_render_nodes_rec(object* node, const object* curr_obj) {
	if (curr_obj == node)
		return;
	if (ImGui::Selectable(node->m_name.c_str(), m_parent_change == node))
		m_parent_change = node;
	for(object* obj : node->m_children)
		ui_render_nodes_rec(obj, curr_obj);
}

/*!
 * \brief changes parent for object given 
 */
bool scene::ui_change_parent(object*& out_obj, const object* curr_obj) {
	m_parent_change = nullptr;
	if (!ImGui::CollapsingHeader("Change Parent")) {
		out_obj = m_parent_change;
		return false;
	}
	if (ImGui::Selectable("Set As Root", false)) {
		m_parent_change = nullptr;
		return true;
	}
	for (object* obj : m_objects) {
		ui_render_nodes_rec(obj, curr_obj);
	}
	out_obj = m_parent_change;
	if (m_parent_change == nullptr)
		return false;
	return true;
}
