#pragma once

#include <glm/glm.hpp>

namespace interpolation {
	glm::vec3 linear(const glm::vec3& x, const glm::vec3& y, const float t);
	glm::quat linear(const glm::quat& x, const glm::quat& y, const float t, bool normalize = false);
	glm::vec3 cubic_hermite(const glm::vec3& x, const glm::vec3& t_x, const glm::vec3& y, const glm::vec3& t_y, const float t);
	glm::vec3 cubic_bezier(const glm::vec3& x, const glm::vec3& y, const glm::vec3& z, const glm::vec3& w, const float t);
	glm::vec3 cubic_catmull(const glm::vec3& x, const glm::vec3& t_x, const glm::vec3& y, const glm::vec3& t_y, const float t);
}