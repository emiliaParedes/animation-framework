#pragma once

#include <string>
#include <vector>
#include <glm/glm.hpp>

class skeleton;

class bone {
	public:
		bone(skeleton* skelly);
		std::string m_name{};
		unsigned m_id = 0;
		//children? yesss 
		std::vector<unsigned> m_children{};		//to m_bones
		//std::vector<bone> m_children;		//to m_bones
		skeleton* m_skeleton = nullptr;
		unsigned m_parent = 0;
		glm::mat4 m_local_bind{};			//read from file
		glm::mat4 m_inverse_bind{};		// calculated from parent
		glm::mat4 m_final_bind{};			//this is the final oh shii
		void calculate_inverse_bind(const glm::mat4& parent);
};