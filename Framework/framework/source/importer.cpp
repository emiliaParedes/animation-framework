/*!
 * \file  importer.cpp
 * \brief This is a file that contains definition of functions for class importer
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "importer.h"

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include "resources.h"
#include "mesh.h"
#include "object.h"
#include "skeleton.h"
#include "animation.h"
#include "animator.h"
#include "scene.h"
#include "animation_channel.h"

namespace {
	//helper ftw
	glm::mat4 ai_to_glm_mat4(aiMatrix4x4& in_mat)
	{
		glm::mat4 tmp;
		tmp[0][0] = in_mat.a1;
		tmp[1][0] = in_mat.b1;
		tmp[2][0] = in_mat.c1;
		tmp[3][0] = in_mat.d1;

		tmp[0][1] = in_mat.a2;
		tmp[1][1] = in_mat.b2;
		tmp[2][1] = in_mat.c2;
		tmp[3][1] = in_mat.d2;

		tmp[0][2] = in_mat.a3;
		tmp[1][2] = in_mat.b3;
		tmp[2][2] = in_mat.c3;
		tmp[3][2] = in_mat.d3;

		tmp[0][3] = in_mat.a4;
		tmp[1][3] = in_mat.b4;
		tmp[2][3] = in_mat.c4;
		tmp[3][3] = in_mat.d4;
		return tmp;
	}
}


 /*!
  * \brief gets a color from material
  */
bool importer::get_material_parameter(aiMaterial* material, const char* type, int, int, aiColor3D* out) {
	if (AI_SUCCESS != material->Get(AI_MATKEY_COLOR_DIFFUSE, out)) {
		//std::cout << "Material doesn't have " << type << " parameter." << std::endl;
		return false;
	}
	return true;
}

/*!
 * \brief gets a float from material
 */
bool importer::get_material_parameter(aiMaterial* material, const char* type, int, int, float* out) {
	if (AI_SUCCESS != material->Get(AI_MATKEY_COLOR_DIFFUSE, out)) {
		//std::cout << "Material doesn't have " << type << " parameter." << std::endl;
		return false;
	}
	return true;
}

/*!
 * \brief gets textures from materials
 */
void importer::get_material_textures(aiMaterial* material, const std::string& path_base) {
	aiString texturePath{};
	auto m_textures = res_man()->get_textures();
	auto m_materials = res_man()->get_materials();

	for (int k = 1; k < aiTextureType_BASE_COLOR; k++) {
		unsigned int numTextures = material->GetTextureCount(static_cast<aiTextureType>(aiTextureType_NONE + k));
		for (unsigned y = 0; y < numTextures; y++) {
			material->GetTexture(static_cast<aiTextureType>(aiTextureType_NONE + k), y, &texturePath);
			std::string texture_name{ texturePath.data };
			size_t pos = texture_name.find_first_of('/') + 1;
			texture_name = texture_name.substr(pos);

			std::string texture_path{texturePath.data};
			pos = texture_path.find_last_of('/') + 1;
			texture_path = texture_path.substr(pos);
			if (m_textures.find(texture_path) == m_textures.end())
				m_textures.insert(std::make_pair(texture_path, new texture(path_base + texture_name)));
			m_materials.back()->m_textures.insert(std::make_pair(static_cast<e_texture_type>(k + y), m_textures.at(texture_path)));
		}
	}
}

/*!
 * \brief gets the mesh from gltf
 */
mesh* importer::process_mesh(unsigned mat_offset, aiMesh* curr_mesh, const aiScene* curr_scene) {
	auto& m_materials = res_man()->get_materials();
	auto& m_meshes = res_man()->get_meshes();
	auto& m_textures = res_man()->get_textures();

	mesh* new_mesh = new mesh{};
	new_mesh->m_shader = 0;				//base 
	new_mesh->m_material = m_materials[mat_offset + curr_mesh->mMaterialIndex];

	//indices
	new_mesh->m_indices.reserve(curr_mesh->mNumFaces * 3);
	for (size_t j = 0; j < curr_mesh->mNumFaces; j++) {
		//current face
		const aiFace& face = curr_mesh->mFaces[j];
		for (int k = 0; k < 3; k++)
			new_mesh->m_indices.push_back(*(face.mIndices + k));
	}
	new_mesh->m_faces = curr_mesh->mNumFaces;


	//vertex init 
	new_mesh->m_vertex.reserve(curr_mesh->mNumVertices);


	//vertices
	//new_mesh->m_positions.reserve(curr_mesh->mNumVertices);
	if (curr_mesh->HasPositions()) {
		for (size_t j = 0; j < curr_mesh->mNumVertices; j++) {
			const aiVector3D* vec = curr_mesh->mVertices + j;
			//new_mesh->m_positions.push_back(glm::vec3(vec->x, vec->y, vec->z));
			new_mesh->m_vertex.push_back({});
			new_mesh->m_vertex.back().m_position = glm::vec3(vec->x, vec->y, vec->z);
			const unsigned colors = curr_mesh->GetNumColorChannels();
			if (colors > 0) {
				const aiColor4D* color = curr_mesh->mColors[0];
				new_mesh->m_vertex.back().m_color = glm::vec4(color->r, color->g, color->b, color->a);
			}
		}
	}

	//normals
	//new_mesh->m_normals.reserve(curr_mesh->mNumVertices);
	if (curr_mesh->HasNormals()) {
		for (size_t j = 0; j < curr_mesh->mNumVertices; j++) {
			const aiVector3D* vec = curr_mesh->mNormals + j;
			//new_mesh->m_normals.push_back(glm::vec3(vec->x, vec->y, vec->z));
			new_mesh->m_vertex[j].m_normal = glm::vec3(vec->x, vec->y, vec->z);
		}
	}

	//texture coords 
	//new_mesh->m_tex_coord.reserve(curr_mesh->mNumVertices * 2);
	if (curr_mesh->HasTextureCoords(0)) {
		for (size_t j = 0; j < curr_mesh->mNumVertices; j++) {
			const aiVector3D& vec = curr_mesh->mTextureCoords[0][j];
			//new_mesh->m_tex_coord.push_back(glm::vec2(vec->x, vec->y));
			new_mesh->m_vertex[j].m_texure_uv = glm::vec2(vec.x, vec.y);
		}
	}

	//skeletal stuff		SHPOOKY
	if (curr_mesh->HasBones()) {
		//create the skin for mesh
		skeleton* skelly_data = new skeleton();	//bone data
		for (unsigned i = 0; i < curr_mesh->mNumBones; i++) {
			//name of bone
			auto& curr_bone = curr_mesh->mBones[i];
			std::string name = curr_bone->mName.data;
			bone* b = new bone(skelly_data);
			b->m_name = name;
			//skin for mesh is done 
			//here we upload the info of each weight according to bone
			unsigned bone_idx = 0;
			//check if inside
			if (new_mesh->m_bone_map.find(name) != new_mesh->m_bone_map.end()) {
				bone_idx = new_mesh->m_bone_map[name];
			}
			else {
				bone_idx = static_cast<unsigned>(new_mesh->m_bone_map.size());
				new_mesh->m_bone_map[name] = bone_idx;
			}
			b->m_id = bone_idx;
			b->m_local_bind = glm::transpose(ai_to_glm_mat4(curr_bone->mOffsetMatrix));
			skelly_data->m_bones.push_back(b);

			//update weight and vertex info 
			//get all weights	
			for (unsigned j = 0; j < curr_bone->mNumWeights; j++) {
				float weight = curr_bone->mWeights[j].mWeight;
				unsigned vtx_idx = curr_bone->mWeights[j].mVertexId;
				for (unsigned k = 0; k < 4; k++) {
					if (new_mesh->m_vertex[vtx_idx].m_weights[k] == 0.0f) {
						new_mesh->m_vertex[vtx_idx].m_weights[k] = weight;
						new_mesh->m_vertex[vtx_idx].m_bone_ids[k] = b->m_id;
						break;
					}
				}
			}
		}
		new_mesh->set_skeleton(skelly_data);

	}

	aiString texturePath{};
	curr_scene->mMaterials[curr_mesh->mMaterialIndex]->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath);
	new_mesh->m_texture = m_textures[texturePath.data];
	new_mesh->upload();
	m_meshes.insert(std::make_pair(std::string(curr_mesh->mName.data), new_mesh));
	return new_mesh;
}

/*!
 * \brief process the object then to mesh
 */
object* importer::process_node(unsigned mat_offset, aiNode* curr_node, const aiScene* curr_scene) {
	//create obj to it 
	object* new_object = new object(curr_node->mName.data);
	aiVector3D pos{}, scale{};
	aiQuaternion rot;
	curr_node->mTransformation.Decompose(scale, rot, pos);
	new_object->m_local.m_position = glm::vec3(pos.x, pos.y, pos.z);
	new_object->m_local.m_scale = glm::vec3(scale.x, scale.y, scale.z);
	new_object->m_local.m_quaternion.w = rot.w;
	new_object->m_local.m_quaternion.x = rot.x;
	new_object->m_local.m_quaternion.y = rot.y;
	new_object->m_local.m_quaternion.z = rot.z;
	//get all meshes in node
	for (unsigned i = 0; i < curr_node->mNumMeshes; i++)
		new_object->m_meshes.push_back(process_mesh(mat_offset, curr_scene->mMeshes[curr_node->mMeshes[i]], curr_scene));
	//get all children nodes
	for (unsigned i = 0; i < curr_node->mNumChildren; i++) {
		object* child = process_node(mat_offset, curr_node->mChildren[i], curr_scene);
		child->m_parent = new_object;
		new_object->m_children.push_back(child);
	}
	return new_object;
}

channel::anim_state get_state(aiAnimBehaviour anim) {
	switch (anim) {
		case aiAnimBehaviour_DEFAULT: {
			return channel::anim_state::anim_default;
			break;
		}
		case aiAnimBehaviour_CONSTANT: {
			return channel::anim_state::anim_constant;
			break;
		}
		case aiAnimBehaviour_LINEAR: {
			return channel::anim_state::anim_linear;
			break;
		}
		case aiAnimBehaviour_REPEAT: {
			return channel::anim_state::anim_repeat;
			break;
		}
	}
}


/*!
 * \brief basic function to call
 */
object* importer::import_file(std::string name) {
	auto& m_materials = res_man()->get_materials();
	auto& m_meshes = res_man()->get_meshes();
	auto& m_textures = res_man()->get_textures();
	const unsigned mat_size = static_cast<unsigned>(m_materials.size());

	const aiScene* scene_ = aiImportFile(name.c_str(),
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	if (scene_ == nullptr) {
		std::cout << name << " was not loaded." << std::endl;
		return nullptr;
	}

	std::string base = name;
	size_t pos = name.find_last_of('/');
	base = base.substr(0, pos + 1);
	//material 
	for (unsigned i = 0; i < scene_->mNumMaterials; i++) {
		aiMaterial* mat = scene_->mMaterials[i];
		m_materials.push_back(new material());

		aiColor3D color{};
		mat->Get(AI_MATKEY_COLOR_DIFFUSE, color);
		m_materials.back()->m_diffuse = glm::vec3(color.r, color.g, color.b);

		mat->Get(AI_MATKEY_COLOR_SPECULAR, color);
		m_materials.back()->m_specular = glm::vec3(color.r, color.g, color.b);

		mat->Get(AI_MATKEY_COLOR_AMBIENT, color);
		m_materials.back()->m_ambient = glm::vec3(color.r, color.g, color.b);

		float ex{};
		mat->Get(AI_MATKEY_SHININESS, ex);
		m_materials.back()->m_shininess = ex;

		mat->Get(AI_MATKEY_SHININESS_STRENGTH, ex);
		m_materials.back()->m_emission = ex;
		
		aiString name;
		mat->Get(AI_MATKEY_NAME, name);

		get_material_textures(mat, base);
	}

	//animations 
	animator* gen_anim = nullptr;
	if (scene_->HasAnimations()) {
		gen_anim = new animator();
		for (unsigned i = 0; i < scene_->mNumAnimations; i++) {
			animation* c_anim = new animation();
			auto& curr_anim = scene_->mAnimations[i];
			c_anim->m_name = curr_anim->mName.data;
			c_anim->m_duration = curr_anim->mDuration;
			c_anim->m_ticks_per_second = curr_anim->mTicksPerSecond;
			for (unsigned j = 0; j < curr_anim->mNumChannels; j++) {
				aiNodeAnim* c_chan = curr_anim->mChannels[j];
				channel* chan = new channel();
				chan->m_target = {c_chan->mNodeName.data};
				//prev and next states
				chan->m_prev_state = get_state(c_chan->mPreState);
				chan->m_next_state = get_state(c_chan->mPostState);

				//positions
				for (unsigned k = 0; k < c_chan->mNumPositionKeys; k++) {
					glm::vec3 pos{  c_chan->mPositionKeys[k].mValue[0],
									c_chan->mPositionKeys[k].mValue[1],
									c_chan->mPositionKeys[k].mValue[2] };
					float t = static_cast<float>(c_chan->mPositionKeys[k].mTime);
					chan->m_positions.push_back(std::make_pair(t, pos));
				}

				//rotations
				for (unsigned k = 0; k < c_chan->mNumRotationKeys; k++) {
					glm::quat rot{};
					rot.w = c_chan->mRotationKeys[k].mValue.w;
					rot.x = c_chan->mRotationKeys[k].mValue.x;
					rot.y = c_chan->mRotationKeys[k].mValue.y;
					rot.z = c_chan->mRotationKeys[k].mValue.z;
					float t = c_chan->mRotationKeys[k].mTime;
					chan->m_quats.push_back(std::make_pair(t, rot));
				}

				//scale
				for (unsigned k = 0; k < c_chan->mNumScalingKeys; k++) {
					glm::vec3 sca{  c_chan->mScalingKeys[k].mValue[0],
									c_chan->mScalingKeys[k].mValue[1],
									c_chan->mScalingKeys[k].mValue[2] };
					float t = c_chan->mPositionKeys[k].mTime;
					chan->m_scalings.push_back(std::make_pair(t, sca));
				}
				c_anim->m_channels.push_back(chan);
				//c_anim->m_object_targets.push_back({c_chan->mNodeName.data});
			}
			
			gen_anim->m_animations.push_back(c_anim);
		}
		
	}
	
	object* n = process_node(mat_size, scene_->mRootNode, scene_);
	n->m_animator = gen_anim;
	return n;
}
