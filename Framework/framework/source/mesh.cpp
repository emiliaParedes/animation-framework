/*!
 * \file  mesh.cpp
 * \brief This is a file that contains definition of functions for class mesh
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "mesh.h"
#include "shader.h"
#include "resources.h"
#include "geometry.h"
#include "scene.h"
#include "light.h"
#include "graphics_manager.h"
#include "skeleton.h"

mesh::mesh()
	: m_vao(0),
	m_vbo(0),
	m_transparent(false),
	m_indices()
{
	create_context();
	bind();
	upload();
	unbind();
}

/*!
 * \brief deletes vaos, ebos and vbos
 */
mesh::~mesh() {
	if (m_skeleton != nullptr)
		delete m_skeleton;
	if (m_vao == 0)
		return;
	glDeleteVertexArrays(1, &m_vao);
	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_ebo);
}

void mesh::set_skeleton(skeleton* new_skelly) {
	if (m_skeleton != nullptr)
		delete m_skeleton;
	m_skeleton = new_skelly;
	if (m_skeleton != nullptr) {
		m_skeleton->m_bone_count = static_cast<unsigned>(m_skeleton->m_bones.size());
		m_skeleton->m_root_bone = 0;
	}
}

/**
* Binds vaos, vbos and ebos
*/
void mesh::bind() {
	//vertex
	glBindVertexArray(m_vao);
	check_gl_error();
	//buffer
	//glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	//check_gl_error();
	//index
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	check_gl_error();
}

/**
* Unbinds
*/
void mesh::unbind() {
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	check_gl_error();

	glBindVertexArray(0);
	check_gl_error();
}

/**
* Creates all vaos, vbos and ebos
*/
void mesh::create_context() {
	//vertex
	glGenVertexArrays(1, &m_vao);
	check_gl_error();
	//buffer
	glGenBuffers(1, &m_vbo);
	check_gl_error();
	//indices
	glGenBuffers(1, &m_ebo);
	check_gl_error();
}

/**
* Uploads graphics information
*/
void mesh::upload() {

	//vertex size 
	GLsizei vertex_size = sizeof(vertex);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	//set data of buffer
	//GLsizeiptr size = static_cast<GLsizeiptr>(m_positions.size() * sizeof(glm::vec3));
	GLsizeiptr size = static_cast<GLsizeiptr>(m_vertex.size() * sizeof(vertex));
	glBufferData(GL_ARRAY_BUFFER, size, m_vertex.data(), GL_STATIC_DRAW);
	check_gl_error();

	//upload format
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	size = static_cast<GLsizeiptr>(m_indices.size() * sizeof(unsigned));
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, m_indices.data(), GL_STATIC_DRAW);
	check_gl_error();

	glBindVertexArray(m_vao);
	//position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, vertex_size, (void*)0);
	check_gl_error();
	glEnableVertexArrayAttrib(m_vao, 0);
	check_gl_error();
	//texture 
	size = sizeof(glm::vec3);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, vertex_size, (void*)size);
	check_gl_error();
	glEnableVertexArrayAttrib(m_vao, 1);
	check_gl_error();
	//color 
	size += sizeof(glm::vec2);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, vertex_size, (void*)size);
	check_gl_error();
	glEnableVertexArrayAttrib(m_vao, 2);
	check_gl_error();
	//normal 
	size += sizeof(glm::vec4);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, vertex_size, (void*)size);
	check_gl_error();
	glEnableVertexArrayAttrib(m_vao, 3);
	check_gl_error();
	//bone ids
	size += sizeof(glm::vec3);
	glVertexAttribIPointer(4, 4, GL_INT, vertex_size, (void*)size);
	check_gl_error();
	glEnableVertexArrayAttrib(m_vao, 4);
	check_gl_error();
	//weights
	size += sizeof(glm::ivec4);
	glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, vertex_size, (void*)size);
	check_gl_error();
	glEnableVertexArrayAttrib(m_vao, 5);
	check_gl_error();
	//tangents
	size += sizeof(glm::vec4);
	glVertexAttribPointer(6, 3, GL_FLOAT, GL_FALSE, vertex_size, (void*)size);
	check_gl_error();
	glEnableVertexArrayAttrib(m_vao, 6);
	check_gl_error();
	//bitangents 
	size += sizeof(glm::vec3);
	glVertexAttribPointer(7, 3, GL_FLOAT, GL_FALSE, vertex_size, (void*)size);
	check_gl_error();
	glEnableVertexArrayAttrib(m_vao, 7);
	check_gl_error();

}

void mesh::bone_transform(float dt, std::vector<glm::mat4> transforms) {
	glm::mat4 identity{ 1.0f };
	//const float ticks_per_s = curr_animation->mTicksPerSecond != 0 ?
	//	m_pScene->mAnimations[0]->mTicksPerSecond : 25.0f;
	//const float time = dt * ticks_per_s;
}

/*!
 * \brief renders the mesh with appropriate shader and material
 */
void mesh::render(const glm::mat4 mat, const glm::mat4 view, const glm::mat4 proj) {
	auto temp = res_man();
	shader* c_shader = temp->get_shaders()[1];
	glUseProgram(c_shader->get_program());
	check_gl_error();

	bind();

	//uniform_mvp
	glm::mat4 m = mat, p = proj, v = view;
	c_shader->set_uniform("m", &m);
	c_shader->set_uniform("v", &v);
	c_shader->set_uniform("p", &p);
	
	if (m_material != nullptr) {
		c_shader->set_uniform("diffuse_color", &m_material->m_diffuse);

		//set texture
		auto& tex = m_material->m_textures[e_texture_type::tex_type_diffuse];

		//auto& tex = m_material->m_textures[e_texture_type::tex_type_diffuse];
		if (tex != nullptr) {
			c_shader->set_uniform("use_vertex_color", 0.0f);
			glActiveTexture(GL_TEXTURE0 + (e_texture_type::tex_type_diffuse - 1));
			glBindTexture(GL_TEXTURE_2D, tex->m_id);
			std::string textureName = "texture_data_" + std::to_string(e_texture_type::tex_type_diffuse - 1);
			c_shader->set_uniform(textureName.c_str(), int(e_texture_type::tex_type_diffuse - 1));
		}
		else
			c_shader->set_uniform("use_vertex_color", 1.0f);
	}



	//spooky stuff
	if (m_skeleton != nullptr && g_gfx_man()->get_skinned()) {
		std::vector<glm::mat4> bone_transform = m_skeleton->get_bone_transforms();
		//if it is not empty 
		//we send this as a uniform to the shader 
		c_shader->set_uniform("skinned", true);
		for (unsigned i = 0; i < bone_transform.size(); i++) {
			std::string base = "bones[";
			c_shader->set_uniform((base + std::to_string(i) + "]").c_str(), &bone_transform[i]);
		}
	} else 
		c_shader->set_uniform("skinned", false);
	//end of spooky stuff



	//glDepthMask(GL_TRUE);
	glEnable(GL_BLEND);
	//glEnable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	check_gl_error();
	glDrawElements(GL_TRIANGLES, static_cast<GLuint>(m_indices.size()), GL_UNSIGNED_INT, 0);
	check_gl_error();
	//glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	unbind();
}

/*!
 * \brief renders objs as lines
 */
void mesh::render_lines(const glm::mat4 mat, const glm::mat4 view, const glm::mat4 proj, glm::vec3 color) {
	auto temp = res_man();
	shader* c_shader = temp->get_shaders()[0];
	glUseProgram(c_shader->get_program());
	check_gl_error();

	bind();

	//uniform_mvp
	glm::mat4 mvp = proj * view * mat;
	c_shader->set_uniform("mvp", &mvp);
	c_shader->set_uniform("diffuse", &color);
	c_shader->set_uniform("use_vertex_color", 1.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawElements(GL_TRIANGLES, static_cast<GLuint>(m_indices.size()), GL_UNSIGNED_INT, 0);
	unbind();
}

