#pragma once

#include <vector>
#include <glm/glm.hpp>
#include <string>
#include <assimp/anim.h>
#include <map>

#include "transform.h"

class object;

struct channel {
	enum class anim_state { anim_default, anim_constant, anim_linear, anim_repeat};
	enum class curve_type { c_linear, c_bezier, c_hermite, c_catmull };

	std::string m_target;	//target
	anim_state m_prev_state = anim_state::anim_default;
	anim_state m_next_state = anim_state::anim_default;
	curve_type m_interpolation = curve_type::c_linear;

	typedef std::pair<float, glm::vec3> key_frame;
	typedef std::pair<float, glm::quat> key_frame_q;

	std::vector<key_frame> m_positions{};
	std::vector<key_frame> m_scalings{};
	std::vector<key_frame_q> m_quats{};
	
	transform m_prev_key{};
	transform m_next_key{};

	unsigned m_key_count = 0;

	//this one should interpolate stuff
	void initialize();
	transform get_transform(const float& t);	//get can do a get_p/get_s/get_q
	unsigned get_keyframe_count()const;

	glm::vec3 get_position(const float& t);
	glm::vec3 get_scale(const float& t);
	glm::quat get_quat(const float& t);

private:
	void map_time(float t);

};
