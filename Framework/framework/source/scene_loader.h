/*!
 * \file  scene_loader.h
 * \brief This is a file that contains declaration off function scene_loader to load scenes into a vector of objs
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <string>

class camera;
class object;

bool scene_loader(const std::string& filename, camera* camera, std::vector<object*>& objects);
