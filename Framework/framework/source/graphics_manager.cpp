/*!
 * \file  graphics_manager.cpp
 * \brief This is a file that contains definition of functions for class graphics_manager
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "graphics_manager.h"

//graphics 
#include "mesh.h"
#include "camera.h"
#include "resources.h"
#include "scene.h"
#include "object.h"
#include "shader.h"
#include "gizmo.h"
#include "geometry.h"
#include "light.h"

//std
#include <iostream>
#include <vector>

//imgui 
#include "..\dependencies\src\imgui\imgui.h"
#include "..\dependencies\src\imgui\imgui_impl_glfw.h"
#include "..\dependencies\src\imgui\imgui_impl_opengl3.h"

//json
#include "../dependencies/src/jsoncpp/json.hpp"

graphics_manager* graphics_manager::g_gfx = nullptr;

namespace {

	/**
	* window resize callback
	* @param GLFWindow* current window
	* @param width - new width
	* @param height - new height
	*/
	void window_size_callback(GLFWwindow* window, int width, int height) {
		//callback to rezie
		glViewport(0, 0, width, height);
		//glfwSetWindowSize(window, width, height); 
		graphics_manager::get_gfx_man()->set_width(width);
		graphics_manager::get_gfx_man()->set_height(height);
	}

	/**
	* mouse input callback
	* @param GLFWindow* current window
	* @param pos mouse in x
	* @param pos mouse in y
	*/
	void mouse_pos_callback(GLFWwindow*, double xpos, double ypos) {
		graphics_manager* gfx = g_gfx_man();
		camera* cam = gfx->get_camera();
		//change in positions
		glm::vec2 mouse_pos{ xpos, ypos };
		glm::vec2 mouse_delta = (g_gfx_man()->get_last_mouse_pos() - mouse_pos) * 0.5f;
		mouse_delta.x *= -1;
		gfx->set_mouse_pos(mouse_pos);
		cam->rotate(mouse_delta);
		gfx->get_gizmo()->update_mouse_pos(xpos,ypos);
	}

	/*!
	 * \brief all key inputs here
	 */
	void key_callback(GLFWwindow* w, int key, int scancode, int action, int mods) {

		//camera and gizmo
		if (key == GLFW_KEY_V)
			g_gfx_man()->get_camera()->reset();
		if (key == GLFW_KEY_Q)
			g_gfx_man()->get_gizmo()->set_operation(0);
		if (key == GLFW_KEY_W)
			g_gfx_man()->get_gizmo()->set_operation(1);
		if (key == GLFW_KEY_E)
			g_gfx_man()->get_gizmo()->set_operation(2);

		//reload and quit
		if (key == GLFW_KEY_ESCAPE || (key == GLFW_KEY_Q && glfwGetKey(w, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)) {
			g_gfx_man()->close();
			return;
		}
		if (key == GLFW_KEY_R && glfwGetKey(w, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS && glfwGetKey(w, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
			res_man()->reload_shaders();
			g_gfx_man()->get_scene()->load_curves();
			return;
		}
		if (key == GLFW_KEY_R && glfwGetKey(w, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
			g_gfx_man()->reload_scene();
			return;
		}

		//camera movement
		g_gfx_man()->get_camera()->move(key, action);
	}

	/*!
	 * \brief mouse input here
	 */
	void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
		//just cam movemenet
		camera* cam = g_gfx_man()->get_camera();
		if (action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_RIGHT)
			cam->m_rotate = true;
		else
			cam->m_rotate = false;
		if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS && !ImGui::IsAnyWindowHovered()) {
			//lock object 
			g_gfx_man()->get_gizmo()->lock();
		}
	}
}

/**
* Initialization of everything needed to render
*/
graphics_manager::graphics_manager() 
	: m_camera(nullptr)
{
	//initialize glfw
	if (!glfwInit())
		exit(-1);
	//just set options 
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	m_camera = new camera();
	m_resources = resource_manager::get_resource_man();
}

/**
* closes glfw and deletes allocated memory
*/
graphics_manager::~graphics_manager() {
	glfwDestroyWindow(m_window);
	//terminate glfw
	glfwTerminate();
	//delete allocated memory
	if (g_gfx != nullptr)
		delete g_gfx;
	g_gfx = nullptr;

	if (m_camera != nullptr)
		delete m_camera;
	m_camera = nullptr;
	if (m_scene != nullptr)
		delete m_scene;
}

/**
* creates window 
*/
void graphics_manager::initialize() {
	//create the window 
	if (m_window == nullptr) {
		m_window = glfwCreateWindow(m_width, m_height, "Potato", NULL, NULL);
		//if error 
		if (m_window == nullptr) {
			glfwTerminate();
			std::cout << "Failed to open window." << std::endl;
			exit(-1);
		}
	}

	//set window context, resize callback and cursor callback 
	glfwMakeContextCurrent(m_window);
	glfwSetFramebufferSizeCallback(m_window, window_size_callback);
	glfwSetCursorPosCallback(m_window, mouse_pos_callback);
	glfwSetKeyCallback(m_window, key_callback);
	glfwSetMouseButtonCallback(m_window, mouse_button_callback);

	//load glad 
	if (!gladLoadGL()) {
		std::cout << "Failed to load OpenGL Context." << std::endl;
		exit(-1);
	}
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cout << "Failed to load OpenGL Context." << std::endl;
		exit(-1);
	}

	//version 
	if (!GLAD_GL_VERSION_4_4) {
		std::cout << "Failed to load OpenGL Context." << std::endl;
		exit(-1);
	}

	//imgui
	ImGui::CreateContext();
	ImGui::StyleColorsDark();
	ImGui_ImplGlfw_InitForOpenGL(m_window, true);
	const char* version = "#version 130";
	ImGui_ImplOpenGL3_Init(version);

	//fill with the initial scene
	m_scenes.push_back(std::make_pair("assets/buggy/Buggy.gltf", false));
	m_scenes.push_back(std::make_pair("assets/box/Box.gltf", false));
	m_scenes.push_back(std::make_pair("assets/buster_drone/scene.gltf", false));
	m_scenes.push_back(std::make_pair("assets/sponza/Sponza.gltf", false));
	m_scenes.push_back(std::make_pair("assets/skull/skull.gltf", false));
	m_scenes.push_back(std::make_pair("assets/waterbottle/WaterBottle.gltf", false));
	m_scenes.push_back(std::make_pair("assets/rigged figure/CesiumMan.gltf", true));
	m_scenes.push_back(std::make_pair("assets/rigged figure/RiggedFigure.gltf", false));
	m_scenes.push_back(std::make_pair("assets/rigged figure/RiggedSimple.gltf", false));
	m_scenes.push_back(std::make_pair("assets/skeleton_king/skeleton_king.gltf", false));
	
	//resources 
	if(m_scene == nullptr)
		m_scene = new scene();
	load_scene();
	if (m_gizmo == nullptr)
		m_gizmo = new gizmo();
	glEnable(GL_DEPTH_TEST);

}

/*!
 * \brief clears window
 */
void graphics_manager::clear() {
	glClearColor(0.16f, 0.16f, 0.15f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT);
}

/*!
 * \brief updates all parameters 
 */
void graphics_manager::update() {
	//delta time 
	float current_frame = static_cast<float>(glfwGetTime());
	m_dt = current_frame - m_last_dt;
	m_last_dt = current_frame;

	//new frame stuff 
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	//updadte camera
	m_camera->update(m_dt);
	
	m_scene->update(m_dt);

	m_gizmo->update(m_dt);
	m_gizmo->get_curve_intersection(m_scene);

}

/*!
 * \brief renders scene and ui
 */
void graphics_manager::render() {
	//camera stuff 


	//render objs 
	//camera 
	const glm::mat4 view = m_camera->get_view_mat();
	const glm::mat4 proj = m_camera->get_proj_mat();

	std::map<std::string, mesh*>& meshes = m_resources->get_meshes();

	std::vector<object*>& objs = m_scene->m_objects;

	//set light information 
	shader* c_shader = m_resources->get_shaders()[1];	//1 we only have 2 so why not
	glUseProgram(c_shader->get_program());
	//c_shader->set_uniform("camera_pos", &m_camera->m_position);

	//loop through all the lights 
	std::vector<light*>& lights = m_scene->m_lights;
	for (int j = 0; j < lights.size(); j++) {
		//sending type of light 
		c_shader->set_uniform("light_position", &lights[j]->m_transform.m_position);
		c_shader->set_uniform("light_color", &lights[j]->m_color);
		c_shader->set_uniform("light_intensity", lights[j]->m_constant);
	}

	//render all objs
	for (object* obj : objs) {
		obj->render(view, proj);
	}


	m_scene->render();

	//gizmo 
	m_gizmo->render(view, proj);

	//imgui 
	render_ui();

	//imgui 
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

	//swap window
	glfwSwapBuffers(m_window);
	glfwPollEvents();

}

/*!
 * \brief render ui 
 */
void graphics_manager::render_ui() {
	bool show_win = true;

	ImGui::Begin("Hello There ", &show_win, {});

	ImGui::Text("General Kenobi");

	ImGui::Text("Spooky Options");
	
	ImGui::NewLine();
	if (ImGui::CollapsingHeader("Spooky Controllers")) {
		ImGui::TextColored(ImVec4(0.19f, 0.625, 0.656f, 1.0f), "To Edit An Object From The List:");
		ImGui::TextColored(ImVec4(0.19f, 0.625, 0.656f, 1.0f), "Right Click On It.");
		ImGui::TextColored(ImVec4(0.19f, 0.625, 0.656f, 1.0f), "Needs To Be Opened.");
	}
	ImGui::NewLine();

	if (ImGui::CollapsingHeader("Spooky New Object")) {
		std::string new_obj{};
		for (std::pair<std::string, bool>& node_scene : m_scenes) {
			std::string name = node_scene.first;
			size_t pos = name.find_last_of('/');
			name = name.substr(pos + 1);
			pos = name.find_first_of('.');
			name = name.substr(0, pos);

			if (ImGui::Selectable(name.c_str(), new_obj == node_scene.first)) {
				new_obj = node_scene.first;
				node_scene.second = true;
				m_scene->load_scene(new_obj);
			}
		}
	}
	ImGui::NewLine();

	if (ImGui::CollapsingHeader("Spooky Scene")) {
		std::string new_obj{};
		for (std::pair<std::string, bool>& node_scene : m_scenes) {
			std::string name = node_scene.first;
			size_t pos = name.find_last_of('/');
			name = name.substr(pos + 1);
			pos = name.find_first_of('.');
			name = name.substr(0, pos);

			if (ImGui::Selectable((name+" ").c_str(), new_obj == node_scene.first)) {
				for (auto& c_n : m_scenes)
					c_n.second = false;
				new_obj = node_scene.first;
				node_scene.second = true;
				reload_scene();
			}
		}
	}
	ImGui::NewLine();

	if (ImGui::CollapsingHeader("Spooky Options")) {
		ImGui::DragFloat("Camera Speed", &m_scene->m_camera->m_speed);
		ImGui::Checkbox("Skinned", &m_skinned);
		ImGui::DragFloat("Joint Size", &m_scene->m_joint_size);
	}
	ImGui::NewLine();

	m_scene->ui_render();

	ImGui::End();
}

/*!
 * \brief reload the entire scene
 */
void graphics_manager::reload_scene() {
	m_scene->unload_scene();
	m_resources->unload();
	load_scene();
	m_gizmo->reset();
}

/*!
 * \brief loads the scene
 */
void graphics_manager::load_scene() {
	m_resources->initialize();
	std::vector<std::string> to_load;
	for (std::pair<std::string, bool>& scenes : m_scenes) {
		if (scenes.second)
			to_load.push_back(scenes.first);
	}
	m_scene->load_scene(to_load);
}

/*!
 * \brief renders a simple cube
 */
void graphics_manager::render_cube(const bounding_volume& bv, const glm::vec4& color) {
	glm::vec3 size_bv = bv.m_max - bv.m_min;
	glm::vec3 pos = bv.m_min + (size_bv * 0.5f);
	glm::mat4 trans{ 1.0f };
	trans = translate(trans, pos);
	trans = scale(trans, size_bv);
	const glm::mat4 view = m_camera->get_view_mat();
	const glm::mat4 proj = m_camera->get_proj_mat();
	m_resources->m_meshes["bv"]->render_lines(trans, view, proj, color);
}

/*!
 * \brief renders a simple cube with point and size
 */
void graphics_manager::render_cube(const glm::vec3& pos, const glm::vec3& scale, const glm::vec4& color) {
	bounding_volume bv{};
	bv.m_min = pos - scale;
	bv.m_max = pos + scale;
	render_cube(bv, color);
}

/*!
 * \brief renders a line
 */
void graphics_manager::render_line(const glm::vec3& p0, const glm::vec3& p1, const glm::vec4& color) {
	glm::mat4 trans{ 1.0f };
	trans = translate(trans, p0);
	trans = scale(trans, p1 - p0);
	glm::mat4 view = m_camera->get_view_mat();
	glm::mat4 proj = m_camera->get_proj_mat();
	m_resources->m_meshes["line"]->render_lines(trans, view, proj, color);
}




