/*!
 * \file  mesh.h
 * \brief This is a file that contains declaration of class mesh
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <glad/glad.h>
#include <unordered_map>
#include "geometry.h"

class shader;
class material;
class texture;
class skeleton;

class mesh {
	public:
		
		std::vector<vertex> m_vertex;
		std::vector<unsigned> m_indices;
		//spooky part
		std::unordered_map<std::string, unsigned> m_bone_map;
		skeleton* m_skeleton = nullptr;
		//end of spooky part
		material* m_material;
		texture* m_texture;
		GLuint m_vao;
		GLuint m_vbo;
		GLuint m_ebo;
		unsigned m_shader;
		unsigned m_faces;
		bool m_transparent;
		mesh();
		~mesh();
		void set_skeleton(skeleton* new_skelly);
		void bind();
		void unbind();
		void create_context();
		void upload();
		void bone_transform(float dt, std::vector<glm::mat4> transforms);
		void render(const glm::mat4 mat, const glm::mat4 view, const glm::mat4 proj);
		void render_lines(const glm::mat4 mat, const glm::mat4 view, const glm::mat4 proj, glm::vec3 color);
};
