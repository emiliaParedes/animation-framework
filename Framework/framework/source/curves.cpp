#include "curves.h"
#include "../dependencies/src/jsoncpp/json.hpp"
#include "graphics_manager.h"
#include "geometry.h"
#include "interpolation.h"
#include "scene.h"

#include <fstream>
#include <iostream>

//helper function to read the file of the curve
namespace {
	using json = nlohmann::json;
	std::string read_curve(const picewise_curve::curve_type type, const std::string& name, std::vector<curve_point>& points, std::vector<support_point>& control) {
		nlohmann::json j;
		std::ifstream in(name.c_str(), std::ios::in);
		if (!in) {
			std::cout << "Cannot open " << name << std::endl;
			return {};
		}
		in >> j;

		//start reading
		json& key_frames = j["keyframes"];
		if (type == picewise_curve::curve_type::pwc_linear || type == picewise_curve::curve_type::pwc_catmull) {
			for (json& kf : key_frames) {
				curve_point p{};
				p.m_distance = kf["time"];
				json& val = kf["vals"];
				p.m_point = { val[0], val[1], val[2] };
				points.push_back(p);
			}
		}
		else {
			int j = 0;
			int p_i = 0;
			int t_i = 1;
			for (int i = 0; i < key_frames.size() - 1; i+=2, j++) {
				p_i = i;
				t_i = i + 1;

				if (j % 2) {
					std::swap(p_i, t_i);
					--i;
				}

				json kf_1 = key_frames[p_i];
				json kf_2 = key_frames[t_i];

				curve_point p{};
				p.m_distance = kf_1["time"];
				json val = kf_1["vals"];
				p.m_point = { val[0], val[1], val[2] };
				points.push_back(p);

				support_point s{};
				json val_1 = kf_2["vals"];
				s.m_tangent = { val_1[0], val_1[1], val_1[2] };
				control.push_back(s);
			}

		}
		std::string n = j["type"];
		return n;
	}
}

/*!
 * \brief constructor
 */
picewise_curve::picewise_curve(const curve_type type) {
	//read file depending on type
	m_type = type;
	switch (m_type) {
		case pwc_linear: {
			m_name = read_curve(type, m_curve_names[0], m_points, m_support);
			break;
		}
		case pwc_bezier: {
			m_name = read_curve(type, m_curve_names[1], m_points, m_support);
			break;
		}
		case pwc_hermite: {
			m_name = read_curve(type, m_curve_names[2], m_points, m_support);
			break;
		}
		case pwc_catmull: {
			m_name = read_curve(type, m_curve_names[3], m_points, m_support);
			break;
		}
	}
}

/*!
 * \brief samples on time
 */
glm::vec3 picewise_curve::sample(float u) const{
	//do nothing if ourside boundries
	if (u < 0.0f)
		return m_points[0].m_point;
	if (u > 1.0f)
		return m_points.back().m_point;
	float curve_range = 0.0f;
	if(m_type == pwc_linear || m_type == pwc_catmull)
		curve_range = 1.0f / (m_points.size() - 1);
	else 
		curve_range = 1.0f / (m_points.size() / 2.0f);
	
	//get the index
	unsigned curve_index = floor(u / curve_range);
	float local_u = (u - curve_index * curve_range) / curve_range;
	//time on local segment

	//get prev and next point (I am not sure this is the best way of doing it but ill fix it in a bit if its wrong)
	if (curve_index > 1 && m_type != pwc_catmull && m_type != pwc_linear)
		curve_index++;
	glm::vec3 prev = m_points[curve_index].m_point;

	//index of the next point
	int idx_next = curve_index + 1;
	//clamp it just in case
	idx_next = glm::clamp(idx_next, 0, static_cast<int>(m_points.size() - 1));
	glm::vec3 next = m_points[idx_next].m_point;
	//if the next one is repeted ignore it 
	if (next == prev && m_type != pwc_catmull)
		next = m_points[++idx_next].m_point;
	//depending on type of curve call the ppropriate interpolation
	switch (m_type) {
		case pwc_linear: {
			//just the linear
			return interpolation::linear(prev, next, local_u);
			break;
		}
		case pwc_bezier: {
			//increase the curve and get the next support
			if (curve_index != 0)
				++curve_index;
			glm::vec3 c_0 = m_support[curve_index].m_control_point;
			glm::vec3 c_1 = m_support[idx_next].m_control_point;
			return interpolation::cubic_bezier(prev, c_0, next, c_1, local_u);
			break;
		}
		case pwc_hermite: {
			//hermite with support
			glm::vec3 c_0 = m_support[curve_index].m_control_point;
			glm::vec3 c_1 = m_support[idx_next].m_control_point;
			return interpolation::cubic_hermite(prev, c_0, next, c_1, local_u);
			break;
		}
		case pwc_catmull: {
			//the 4 points to send the catmull interpolation
			int idx_0 = curve_index - 1;
			int idx_2 = curve_index + 2;
			idx_0 = glm::clamp(idx_0, 0, static_cast<int>(m_points.size() - 1));
			idx_2 = glm::clamp(idx_2, 0, static_cast<int>(m_points.size() - 1));
			glm::vec3 p0 = m_points[idx_0].m_point;
			glm::vec3 p1 = m_points[curve_index].m_point;
			glm::vec3 p2 = m_points[idx_next].m_point;
			glm::vec3 p3 = m_points[idx_2].m_point;
			return interpolation::cubic_catmull(p0, p1, p2, p3, local_u);
			break;
		}
	}
	//default
	return {};
}

/*!
 * \brief renders a curve depending on type
 */
void picewise_curve::render(unsigned precision, std::pair<int, int> hovered, std::pair<int, int> selected) {
	glm::vec3 sca = g_gfx_man()->get_scene()->m_point_size;
	switch (m_type) {
		case pwc_linear: {
			render_linear();
			break;
		}
		case pwc_bezier: {
			render_bezier(precision);
			break;
		}
		case pwc_hermite: {
			render_hermite(precision);
			break;
		}
		case pwc_catmull: {
			render_catmull(precision);
			break;
		}
	}

	//render eveyting (has a little bit of colors for hovered or selected)
	auto* g = g_gfx_man();
	glm::vec4 color = { 0.5f, 0.3f, 0.5f, 1.0f };
	glm::vec4 hover = { 1.0f, 0.4f, 0.5f, 1.0f };
	glm::vec4 selec = { 0.7f, 0.8f, 0.3f, 1.0f };
	for (int i = 0; i < m_points.size(); i++) {
		curve_point& p = m_points[i];
		if (i != 0)
			if (m_points[i - 1].m_point == p.m_point)
				continue;
		glm::vec4 c{color};
		if (i == hovered.first)
			c = hover;
		if (i == selected.first)
			c = selec;
		bounding_volume bv{};
		bv.m_min = p.m_point - sca;
		bv.m_max = p.m_point + sca;
		g->render_cube(bv, c);
	}
	int i = 0;
	color = { 0.7f, 0.7f, 0.7f, 1.0f };
	for (int i = 0; i < m_support.size(); i++) {
		support_point& s = m_support[i];
		glm::vec4 c{ color };
		if (i == hovered.second)
			c = hover;
		if (i == selected.second)
			c = selec;
		bounding_volume bv{};
		bv.m_min = s.m_control_point - sca;
		bv.m_max = s.m_control_point + sca;
		g->render_cube(bv, c);
		g->render_line(m_points[i].m_point, m_support[i].m_control_point, c);
	}
}

/*!
 * \brief renders linear
 */
void picewise_curve::render_linear() const {
	//render the lines for linear
	glm::vec4 color = { 1.0f, 0.0f, 0.5f, 1.0f };
	auto* g = g_gfx_man();
	for (int i = 0; i < m_points.size() - 1; i++) {
		glm::vec3 p0 = m_points[i].m_point;
		glm::vec3 p1 = m_points[i + 1].m_point;
		g->render_line(p0, p1, color);
	}
}

/*!
 * \brief render w bezier interpolation
 */
void picewise_curve::render_bezier(unsigned precision) const {
	//render the lines for bezier with given precision
	unsigned times = precision;
	auto* g = g_gfx_man();
	glm::vec4 color = { 1.0f, 0.0f, 0.5f, 1.0f };
	for (int i = 0; i < m_points.size() - 1;) {
		glm::vec3 p0 = m_points[i].m_point;
		glm::vec3 t0 = m_support[i].m_tangent;
		glm::vec3 p1 = m_points[++i].m_point;
		glm::vec3 t1 = m_support[i].m_tangent;
		glm::vec3 prev = p0;
		if (m_points[i - 1].m_distance == m_points[i].m_distance)
			continue;
		float t = 0.0f;
		for (unsigned j = 0; j < times; j++) {
			t += 1.0f / static_cast<float>(times);
			glm::vec3 n = interpolation::cubic_bezier(p0, t0, p1, t1, t);
			g->render_line(prev, n, color + t);
			prev = n;
		}
		color[i % 3] = 0.0f;
	}

}

/*!
 * \brief render w hermite interpolation
 */
void picewise_curve::render_hermite(unsigned precision) const {
	//render the lines for hermite w precision
	auto* g = g_gfx_man();
	unsigned times = precision;
	glm::vec4 color = { 0.0f, 0.0f, 0.5f, 1.0f };
	for (int i = 0; i < m_points.size() - 1;) {
		glm::vec3 p0 = m_points[i].m_point;
		glm::vec3 t0 = m_support[i].m_tangent;
		glm::vec3 p1 = m_points[++i].m_point;
		glm::vec3 t1 = m_support[i].m_tangent;
		glm::vec3 prev = p0;
		if (m_points[i - 1].m_distance == m_points[i].m_distance)
			continue;
		float t = 0.0f;
		for (unsigned j = 0; j < times; j++) {
			t += 1.0f / static_cast<float>(times);
			glm::vec3 n = interpolation::cubic_hermite(p0, t0, p1, t1, t);
			g->render_line(prev, n, color + t);
			prev = n;
		}
		color[i % 3] = 0.0f;
	}
}

/*!
 * \brief render w catmull interpolation
 */
void picewise_curve::render_catmull(unsigned precision) const {
	unsigned times = precision;
	auto* g = g_gfx_man();
	glm::vec4 color = { 0.0f, 0.0f, 0.5f, 1.0f };
	int idx_0 = -1;
	int idx_1 = 1;
	int idx_2 = 2;
	float u = 0.0f;
	float delta = 1.0f / static_cast<float>(times);
	glm::vec3 prev = sample(u);
	for (unsigned i = 0; i < precision; i++) {
		u += delta;
		glm::vec3 pos = sample(u);
		g->render_line(prev, pos, color+u);
		prev = pos;
	}
	return;
}
