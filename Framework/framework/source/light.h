/*!
 * \file  light.h
 * \brief This is a file that contains declaration of class light
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once

#include <glm/glm.hpp>
#include "transform.h"

class light {
public:
	light();
	transform m_transform;

	glm::vec3 m_color;
	float m_constant;
	float m_linear;
	float m_quadratic;
};