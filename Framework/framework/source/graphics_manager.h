/*!
 * \file  graphics manager.h
 * \brief This is a file that contains declaration of class graphics_manager. Here is where magic happens.
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once 

#include <vector>
#include <string>

#include <glad/glad.h>
#include <GLFW/glfw3.h>		//window and input	
#include <glm/glm.hpp>


class mesh;
class camera;
class resource_manager;
class light;
class object;
class scene;
class gizmo;
struct bounding_volume;

class graphics_manager {
	
	private:
		graphics_manager();
		int m_width = 1200;
		int m_height = 900;
		GLFWwindow* m_window = nullptr;
		camera* m_camera = nullptr;
		resource_manager* m_resources = nullptr;
		scene* m_scene = nullptr;
		std::vector<mesh*> m_meshes;
		//singleton
		static graphics_manager* g_gfx;
		float m_dt{0.0f};
		glm::vec2 m_last_mouse_pos{};
		float m_last_dt{ 0.0f };
		bool m_closed = false;

		//other 
		gizmo* m_gizmo = nullptr;
		
		//spooky 
		bool m_skinned = true;
		//spooky end

		std::vector<std::pair<std::string, bool>> m_scenes{};

	public: 
		//singleton implementation 
		inline static graphics_manager* get_gfx_man() { if (g_gfx == nullptr) g_gfx = new graphics_manager(); return g_gfx; };

		~graphics_manager();

		void initialize();
		void clear();
		void update();
		void render();
		void render_ui();
		bool closed() { return m_closed; };
		void reload_scene();
		void load_scene();

		//setters and getters 
		//spooky
		inline bool get_skinned() const { return m_skinned; }
		//end spooky
		inline void set_height(const int& height) { m_height = height; }
		inline int get_height() { return m_height; }
		inline void set_width(const int& width) { m_width = width; }
		inline int get_width() { return m_width; }
		camera* get_camera() const { return m_camera; }
		scene* get_scene() const { return m_scene; }
		void set_scene(scene* new_scene) { m_scene = new_scene; }
		gizmo* get_gizmo() const { return m_gizmo; }
		const float get_delta_time() const { return m_dt; }
		const glm::vec2 get_last_mouse_pos() const { return m_last_mouse_pos; }
		void set_mouse_pos(glm::vec2 pos) { m_last_mouse_pos = pos; }
		void close() { m_closed = true; }

		void render_cube(const bounding_volume& bv, const glm::vec4& color);
		void render_cube(const glm::vec3& pos, const glm::vec3& scale, const glm::vec4& color);
		void render_line(const glm::vec3& p0, const glm::vec3& p1, const glm::vec4& color = glm::vec4(1.0f));
		//void RenderTriangle(const glm::mat4& view, const glm::mat4& proj, const triangle& trig, const glm::vec4& color = glm::vec4(1.0f));
		//void RenderBox(const glm::mat4& view, const glm::mat4& proj, const aabb& obj, const glm::vec4 color = glm::vec4(1.0f));
		//void RenderPoint(const glm::mat4& view, const glm::mat4& proj, const glm::vec3 p, const glm::vec4& color = glm::vec4(1.0f));
}; 

#define g_gfx_man() graphics_manager::get_gfx_man()
#define delta_time() graphics_manager::get_gfx_man()->get_delta_time()