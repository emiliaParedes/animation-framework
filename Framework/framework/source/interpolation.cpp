#include "interpolation.h"
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/spline.hpp>

/*!
 * \brief basic linear interpolation
 */
glm::vec3 interpolation::linear(const glm::vec3& x, const glm::vec3& y, const float t) {
	return x + (y - x) * t;
}

/*!
 * \brief basic linear interpolati for quaternions (maybe normalized)
 */
glm::quat interpolation::linear(const glm::quat& x, const glm::quat& y, const float t, bool normalize) {
	//normalize if normalize
	if (normalize)
		return glm::normalize(glm::slerp(x, y, t));
	return glm::slerp(x, y, t);
}

/*!
 * \brief cubic hermite interpolation 
 */
glm::vec3 interpolation::cubic_hermite(const glm::vec3& x, const glm::vec3& t_x, const glm::vec3& y, const glm::vec3& t_y, const float t) {
	const float t_sq = t * t;
	const float t_cb = t_sq * t;
	return  (2.0f * t_cb - 3.0f * t_sq + 1.0f) * x + 
		 	(t_cb - 2.0f * t_sq + t) * t_x + 
			(-2.0f * t_cb + 3.0f * t_sq) * y + 
			(t_cb - t_sq) * t_y;
}

/*!
 * \brief cubic bezier interpolation
 */
glm::vec3 interpolation::cubic_bezier(const glm::vec3& x, const glm::vec3& y, const glm::vec3& z, const glm::vec3& w, const float t) {
	const float diff = (1.0f - t);
	const float diff_sq = diff * diff;
	const float t_sq = t * t;
	return  diff_sq * diff * x + 
			3.0f * diff_sq * t * y + 
			3.0f * diff * t_sq * w + 
			t_sq * t * z;
}

/*!
 * \brief cubic catmull interpolation
 */
glm::vec3 interpolation::cubic_catmull(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3, const float t) {
	glm::vec3 result{};
	float t2 = t * t;
	float t3 = t2 * t;
	return  t3 * ( (-1.0f) * p0 + 3.0f * p1 - 3.0f * p2 + p3) / 2.0f +
			t2 * (2.0f * p0 - 5.0f * p1 + 4.0f * p2 - p3) / 2.0f +
			t * ((-1.0f) * p0 + p2) / 2.0f +
			p1;
}
