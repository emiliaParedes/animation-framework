/*!
 * \file  scene_loader.cpp
 * \brief This is a file that contains definition of function scene_loader
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "scene_loader.h"
#include "camera.h"
#include "object.h"

#include <iostream>
#include <fstream>
#include "../dependencies/src/jsoncpp/json.hpp"
#include <glm/glm.hpp>

using json = nlohmann::json;

/*!
 * \brief simple loader of a json, not for this class :)
 */
bool scene_loader(const std::string& filename, camera* cam, std::vector<object*>& objects)
{
	nlohmann::json j;
	std::ifstream in(filename.c_str(), std::ios::in);
	if (!in) 	{
		std::cout << "Cannot open " << filename << std::endl;
		return false;
	}
	in >> j;

	json& cam_j = j["camera"];
	json& cam_translate = cam_j["translate"];
	cam->m_position.x = cam_translate["x"];
	cam->m_position.x = cam_translate["y"];
	cam->m_position.x = cam_translate["z"];

	json& cam_rotate = cam_j["rotate"];
	cam->m_rotation.x= cam_rotate["x"];
	cam->m_rotation.y= cam_rotate["y"];
	cam->m_rotation.z= cam_rotate["z"];

	cam->m_near = cam_j["near"];
	cam->m_far = cam_j["far"];
	//cam->m_fov = glm::radians(cam_j["FOVy"]);
	cam->m_fov = glm::radians(static_cast<float>(cam_j["FOVy"]));

	json& objs = j["objects"];
	unsigned obj_id = 0;

	for (json& it : objs) 	{
		json& obj_trans = it["translate"];
		json& obj_rot = it["rotate"];
		json& obj_scale = it["scale"];

		transform tr{	glm::vec3(obj_trans["x"], obj_trans["y"], obj_trans["z"]), 
						glm::vec3(obj_scale["x"], obj_scale["y"], obj_scale["z"]), 
						glm::vec3(obj_rot["x"], obj_rot["y"], obj_rot["z"]) };
		std::vector<std::string> mesh_name{};
		std::string temp = it["mesh"];
		size_t pos = temp.find_last_of('/') + 1;
		temp = temp.substr(pos);
		pos = temp.find_last_of('.');
		temp = temp.substr(0, pos);
		mesh_name.push_back(temp);
		objects.push_back(new object(obj_id++, mesh_name, tr));
	}

	return true;
}
