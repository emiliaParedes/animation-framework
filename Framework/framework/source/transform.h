/*!
 * \file  scene.h
 * \brief This is a file that contains declaration of class transform
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

struct transform {
	transform() = default;
	transform(glm::vec3 pos, glm::vec3 scale, glm::quat rot) { m_position = pos; m_scale = scale; m_quaternion = rot; };
	glm::vec3 m_position{ 0.0f };
	glm::vec3 m_scale{ 1.0f };
	glm::quat m_quaternion{};

	glm::mat4 get_transform_mat() const;
};