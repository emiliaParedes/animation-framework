#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include "animation_channel.h"
#include "transform.h"

class object;
class scene;

class animation {
	public:
		~animation(); 
		std::string m_name{};
		double m_duration = 0.0;
		double m_ticks_per_second = 0.0;
		std::vector<object*> m_object_targets{};
		std::vector<channel*> m_channels{};
		
		//prev and next key frame;
		//previous time

		typedef std::unordered_map<std::string, transform> poses;

		void initialize(scene* sc);
		float get_time_from_ticks(float t);

		void update(float dt);
		//could take in cached data yes
		poses produce_poses(float curr_time);
};


