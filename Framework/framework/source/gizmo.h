/*!
 * \file  gizmo.h
 * \brief This is a file that contains declaration of class gizmo
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once

#include <glm/glm.hpp>
#include "../dependencies/src/imguizmo/ImGuizmo.h"

class object;
class scene;

class gizmo {
	public:
		gizmo();
		~gizmo();
		void initialize();
		void update(const float& dt);
		void render(const glm::mat4& view, const glm::mat4 proj);
		void lock();
		void lock(object* new_lock) { m_locked = new_lock; }
		bool in_use();
		void update_mouse_pos(const double x, const double y);
		void set_operation(const int& op);
		void reset();
		void get_curve_intersection(scene* sc);
	private:
		void edit_object(const glm::mat4& view, const glm::mat4& proj) const;
		void edit_object_ui();
		void edit_curve(const glm::mat4& view, const glm::mat4& proj) const;
		void edit_curve_ui();
		object* m_locked;
		object* m_hover;
		glm::vec3 m_mouse;
		bool m_in_use;
		ImGuizmo::OPERATION m_operation;
};