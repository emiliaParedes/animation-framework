//spooky
#pragma once

#include <unordered_map>
#include <string>
#include <vector>
#include <glm/glm.hpp>
#include "bone.h"

class object;

class skeleton {
	public:
		~skeleton();
		std::vector<bone*> m_bones;	
		unsigned m_root_bone = 0;
		unsigned m_bone_count = 0;
		object* m_root = nullptr;
		void initialize();
		void render() const;
		void update_bone_hierarchy(object* o);
		bone* get_bone(const std::string& name);
		unsigned get_bone_idx(const std::string& name);
		void calculate_inverse_bind();
		std::vector<glm::mat4> get_bone_transforms();
	private:
		void render_bones(const unsigned c_bone) const;
};