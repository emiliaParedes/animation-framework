/*!
 * \file  importer.h
 * \brief This is a file that contains declaration of class importer, usage of assimp 
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once

#include <vector>
#include <string>

#include <assimp/cimport.h>
#include <assimp/postprocess.h>	//not sure about this one 
#include <assimp/scene.h>

class resource_manager;
class object;
class mesh;
class scene;

class importer {
	private:
		bool get_material_parameter(aiMaterial* material, const char* type, int, int, aiColor3D* out);
		bool get_material_parameter(aiMaterial* material, const char* type, int, int, float* out);
		void get_material_textures(aiMaterial* material, const std::string& path_base);
		mesh* process_mesh(unsigned mat_offset, aiMesh* curr_mesh, const aiScene* curr_scene);
		object* process_node(unsigned mat_offset, aiNode* curr_node, const aiScene* curr_scene);
	public:
		object* import_file(std::string scenes);
};