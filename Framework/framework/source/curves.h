#pragma once

#include <vector>
#include <glm/glm.hpp>
#include <string>

struct curve_point {
	glm::vec3 m_point; //this can be a tangent too 
	float m_distance; //time
};

struct support_point{
	union {
		glm::vec3 m_tangent;
		glm::vec3 m_control_point;
	};
};

class picewise_curve {
	public:
		enum curve_type { pwc_linear, pwc_bezier, pwc_hermite, pwc_catmull };

		const std::vector<std::string> m_curve_names{	"resources/curves/lines.json.curve",
														"resources/curves/bezier.json.curve",
														"resources/curves/hermite.json.curve",
														"resources/curves/catmull.json.curve"};

		picewise_curve(const curve_type type);

		std::string m_name{};
		curve_type m_type = pwc_linear;
		std::vector<curve_point> m_points{};
		std::vector<support_point> m_support{};
		unsigned segment_num{};

		glm::vec3 sample(float u) const;
		void render(unsigned precision = 1, std::pair<int, int> hovered = {-1, -1}, std::pair<int, int> selected = {-1, -1});
	private:
		void render_linear()  const;
		void render_bezier(unsigned precision)  const;
		void render_hermite(unsigned precision) const;
		void render_catmull(unsigned precision) const;

};

namespace {
	std::string read_curve(const picewise_curve::curve_type type, const std::string& name, std::vector<curve_point>& points, std::vector<support_point>& control);
}

