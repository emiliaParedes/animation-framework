#include "skeleton.h"
#include "transform.h"
#include "object.h"
#include "graphics_manager.h"
#include "scene.h"

#include <iostream>

/*!
 * \brief destructor, frees memory
 */
skeleton::~skeleton() {
	for (bone* b : m_bones)
		delete b;
}

/*!
 * \brief updates hierarchy
 */
void skeleton::initialize() {
	//m_root = root_obj;
	m_root = g_gfx_man()->get_scene()->get_object(m_bones[0]->m_name);
	update_bone_hierarchy(m_root);
	calculate_inverse_bind();
}

/*!
 * \brief renders skelleton
 */
void skeleton::render() const {
	render_bones(m_root_bone);
}

/*!
 * \brief updates the hierarchy of the bones
 */
void skeleton::update_bone_hierarchy(object* o) {
	bone* b = get_bone(o->m_name);
	for (object* child : o->m_children) {
		update_bone_hierarchy(child);
		b->m_children.push_back(get_bone_idx(child->m_name));
	}
}

/*!
 * \brief gets desired bone by name
 */
bone* skeleton::get_bone(const std::string& name) {
	for (bone* b : m_bones) {
		if (b->m_name == name)
			return b;
	}
	return nullptr;
}

/*!
 * \brief gets the index of the vone by given name
 */
unsigned skeleton::get_bone_idx(const std::string& name) {
	for (unsigned i = 0; i < m_bones.size(); i++) {
		if (m_bones[i]->m_name == name)
			return i;
	}
	return 0;
}

/*!
 * \brief inverse bind for bone
 */
void skeleton::calculate_inverse_bind() {
	m_bones[m_root_bone]->calculate_inverse_bind(glm::mat4(1.0f));
}

/*!
 * \brief transforms to send to shader				
 */
std::vector<glm::mat4> skeleton::get_bone_transforms() {
	std::vector<glm::mat4> all_matrices{ m_bone_count };
	const glm::mat4 world_inverse = glm::inverse(m_root->get_world_transform_mat());
	for (bone* b : m_bones) {
		object* obj = g_gfx_man()->get_scene()->get_object(b->m_name);
		if (obj == nullptr)
			continue;
		glm::mat4 bone_world = obj->get_world_transform_mat();
		all_matrices[b->m_id] = world_inverse * bone_world * b->m_local_bind;
	}
	return all_matrices;
}

/*!
 * \brief renders the bones with hierarchy
 */
void skeleton::render_bones(const unsigned c_bone)const {
	bone* b = m_bones[c_bone];
	auto* g = g_gfx_man();
	scene* sc = g->get_scene();
	glm::vec3 pos_0 = sc->get_object(b->m_name)->m_world.m_position;
	glm::vec4 color_bones{ 0.0f, 1.0f, 0.23f, 1.0f };
	glm::vec4 color_joints{ 1.0f, 0.2f, 0.57f, 1.0f };
	glm::vec3 sca{sc->m_joint_size};
	g->render_cube(pos_0, sca, color_joints);
	for (int i = 0; i < b->m_children.size(); i++) {
		glm::vec3 child = sc->get_object(m_bones[b->m_children[i]]->m_name)->m_world.m_position;
		g->render_line(pos_0, child, color_bones);
		render_bones(b->m_children[i]);
	}
}

