#include "animation_channel.h"
#include "interpolation.h"

//debug 
#include "scene.h"
#include "graphics_manager.h"
#include "object.h"

#include <iostream>


/*!
 * \brief initialization of the channel
 */
void channel::initialize() {
	object* target = g_gfx_man()->get_scene()->get_object(m_target);
	m_positions.push_back({0.0f, target->m_local.m_position });
	m_scalings.push_back({0.0f, target->m_local.m_scale });
	m_quats.push_back({0.0f, target->m_local.m_quaternion });

	std::sort(m_positions.begin(), m_positions.end(),
		[](const key_frame& a, const key_frame& b) -> bool {
		return a.first < b.first;
	});
	std::sort(m_scalings.begin(), m_scalings.end(),
		[](const key_frame& a, const key_frame& b) -> bool {
		return a.first < b.first;
	});
	std::sort(m_quats.begin(), m_quats.end(),
		[](const key_frame_q& a, const key_frame_q& b) -> bool {
		return a.first < b.first;
	});
	
	if(m_positions.size() > 1)
		m_next_key.m_position   = m_positions[1].second;
	if (m_scalings.size() > 1)
		m_next_key.m_scale      = m_scalings[1].second;
	if (m_quats.size() > 1)
		m_next_key.m_quaternion = m_quats[1].second;

	m_prev_key.m_position = m_positions[0].second;
	m_prev_key.m_scale = m_scalings[0].second;
	m_prev_key.m_quaternion = m_quats[0].second;

	m_key_count = m_positions.size() > m_scalings.size() ? m_positions.size() : m_scalings.size();
	m_key_count = m_key_count > m_quats.size() ? m_key_count : m_quats.size();

}

/*!
 * \brief transform from time in entire animation
 */
transform channel::get_transform(const float& t) {
	transform out_trans{};
	
	//update the cache if we found another keyframe 
	map_time(t);

	float curve_range = 1.0f / (m_key_count);
	unsigned curve_index = floor(t / curve_range);
	float local_u = (t - curve_index * curve_range) / curve_range;

	out_trans.m_position = get_position(local_u);
	out_trans.m_scale = get_scale(local_u);
	out_trans.m_quaternion = get_quat(local_u);
	return out_trans;
}

/*!
 * \brief getter
 */
unsigned channel::get_keyframe_count() const {
	return m_key_count;
}

/*!
 * \brief gets position from animation from local time
 */
glm::vec3 channel::get_position(const float& t) {
	glm::vec3 new_pos{};
	switch (m_interpolation){
		case curve_type::c_linear: {
			new_pos = interpolation::linear(m_prev_key.m_position, m_next_key.m_position, t);
			break;
		}
		case curve_type::c_bezier: {
			break;
		}
		case curve_type::c_hermite :{
			break;
		}
		case curve_type::c_catmull: {
			break;
		}
	}
	return new_pos;
}

/*!
 * \brief gets scale from animation from local time
 */
glm::vec3 channel::get_scale(const float& t) {
	glm::vec3 new_sca{};
	switch (m_interpolation) {
		case curve_type::c_linear: {
			new_sca = interpolation::linear(m_prev_key.m_scale, m_next_key.m_scale, t);
			break;
		}
		case curve_type::c_bezier: {
			break;
		}
		case curve_type::c_hermite: {
			break;
		}
		case curve_type::c_catmull: {
			break;
		}
	}
	return new_sca;
}

/*!
 * \brief gets quat from animation from local time
 */
glm::quat channel::get_quat(const float& t) {
	return interpolation::linear(m_prev_key.m_quaternion, m_next_key.m_quaternion, t);
}

/*!
 * \brief maps the time depending on the keyframe
 */
void channel::map_time(float t) {
	//position
	for (int i = 0; i < m_positions.size(); i++) {
		if (m_positions[i].first >= t) {
			//update info 
			int prev = i - 1;
			prev = glm::clamp(prev, 0, static_cast<int>(m_positions.size() -1));
			m_prev_key.m_position = m_next_key.m_position;
			m_next_key.m_position = m_positions[i].second;
			break;
		}
	}
	//scale 
	for (int i = 0; i < m_scalings.size(); i++) {
		if (m_scalings[i].first >= t) {
			//update info 
			m_prev_key.m_scale = m_next_key.m_scale;
			m_next_key.m_scale = m_scalings[i].second;
			break;
		}
	}
	//rotation 
	for (int i = 0; i < m_quats.size(); i++) {
		if (m_quats[i].first >= t) {
			//update info 
			m_prev_key.m_quaternion = m_next_key.m_quaternion;
			m_next_key.m_quaternion = m_quats[i].second;
			break;
		}
	}
}

