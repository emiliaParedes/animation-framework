#pragma once

#include <vector>
#include <unordered_map>
#include <glm/glm.hpp>
#include "animation.h"

class scene;

class animator {
	public:

		std::vector<animation*> m_animations;
		int m_curr_animation = 0;
		float m_animation_time = 0.0f;

		void change_animation(unsigned new_anim);
		void update(float dt);
		void initialize(scene* sc);
		void reset_animation();

		bool m_paused = false;
		bool m_loop = false;

		//cached data , per channel?
		unsigned m_prev_key;
		unsigned m_next_key;
		float m_prev_time;

	private:
		void update_anim_time(float dt);
};