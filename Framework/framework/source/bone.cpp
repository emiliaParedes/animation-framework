#include "bone.h"
#include "skeleton.h"

bone::bone(skeleton* skelly)
	: m_skeleton (skelly) 
{}

//this is the parenting in control (i hope)
void bone::calculate_inverse_bind(const glm::mat4& parent) {
	glm::mat4 bind = parent * m_local_bind;
	m_inverse_bind = glm::inverse(bind);
	if (m_skeleton != nullptr) {
		for (unsigned& child : m_children) {
			m_skeleton->m_bones[child]->calculate_inverse_bind(bind);
		}
	}
}
