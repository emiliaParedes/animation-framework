#include "animator.h"
#include "scene.h"

#include <iostream>

/*!
 * \brief changes the animation 
 */
void animator::change_animation(unsigned new_anim) {
	//santy check ftw 
	if (new_anim < m_animations.size())
		m_curr_animation = new_anim;
	m_animation_time = 0.0f;
}

/*!
 * \brief updates animator
 */
void animator::update(float dt) {
	if (m_curr_animation == -1 || m_paused)
		return;
	update_anim_time(dt);

	m_animations[m_curr_animation]->update(m_animation_time);
}

/*!
 * \brief initializes the animations and channels
 */
void animator::initialize(scene* sc) {
	for (animation* a : m_animations)
		a->initialize(sc);
}

/*!
 * \brief resets animation time
 */
void animator::reset_animation() {
	m_animation_time = 0.0f;
}

/*!
 * \brief updates the animation time depending on dt and loop 
 */
void animator::update_anim_time(float dt) {
	m_animation_time += dt;

	if (m_animation_time >= m_animations[m_curr_animation]->m_duration && m_loop)
		m_animation_time = 0.0f;
}
