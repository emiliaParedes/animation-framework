/*!
 * \file  object.cpp
 * \brief This is a file that contains definition of functions for class object
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "object.h"
#include "mesh.h"
#include "resources.h"
#include "graphics_manager.h"
#include "scene.h"
#include "skeleton.h"
#include "animator.h"

#include <glm/gtx/matrix_decompose.hpp>
#include <iostream>

 /*!
  * \brief object
  */
object::object(unsigned id, std::vector<std::string>& mesh_name, transform trans)
	: m_id(id),
	m_world(trans),
	m_local()
{
	resource_manager* res = res_man();
	for (std::string& name : mesh_name)
		m_meshes.push_back(res->m_meshes[name]);
}

/*!
 * \brief destructor
 */
object::~object() {
	detach();
	for (object* obj : m_children)
		delete obj;
	m_meshes.clear();
	if (m_animator != nullptr)
		delete m_animator;
	m_animator = nullptr;
}

/*!
 * \brief bv mainly
 */
void object::initialize() {
	//get the bv 
	m_bv = bv_aabb_from_obj(this);
	m_bv_world = get_world_bv();
	for(mesh*& m : m_meshes){
		if (m->m_skeleton != nullptr) {
			m->m_skeleton->initialize();
		}
	}
	for (object* obj : m_children)
		obj->initialize();
	if (m_animator)
		m_animator->initialize(g_gfx_man()->get_scene());
}

/*!
 * \brief renders mesh if any
 */
void object::render(const glm::mat4& view, const glm::mat4& proj) {
	//get transform then render stuff 
	glm::mat4 transform = m_world.get_transform_mat();
	for (mesh* mesh_ : m_meshes) {
		if (m_visible) {
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			mesh_->render(transform, view, proj);
			glDisable(GL_CULL_FACE);
		}
		if (mesh_->m_skeleton != nullptr && m_render_skelly) {
			mesh_->m_skeleton->render();
		}
	}
	for (object* node : m_children)
		node->render(view, proj);

}

/*!
 * \brief updates world
 */
void object::update(const float& dt) {
	if (m_animator)
		m_animator->update(dt);
	calculate_world();
	for (object* node : m_children)
		node->update(dt);
}

/*!
 * \brief calculates world from parent if any
 */
void object::calculate_world() {
	if (m_parent == nullptr) {
		m_world = m_local;
		return;
	}
	m_world.m_position = m_parent->m_world.get_transform_mat() * glm::vec4(m_local.m_position, 1.0f);
	m_world.m_scale = m_parent->m_world.m_scale * m_local.m_scale;
	m_world.m_quaternion = glm::normalize(m_parent->m_world.m_quaternion * m_local.m_quaternion);
}

/*!
 * \brief world transform
 */
glm::mat4 object::get_world_transform_mat() const {
	return m_world.get_transform_mat();
}

/*!
 * \brief local transform
 */
glm::mat4 object::get_local_transform_mat() const {
	return m_local.get_transform_mat();
}

/*!
 * \brief updates localtransform
 */
void object::update_local(const transform& new_local) {
	m_local = new_local;
	m_bv_world = get_world_bv();
}

/*!
 * \brief gets the new bv from world
 */
bounding_volume object::get_world_bv() const {
	if (m_meshes.empty())
		return m_bv;
	return bv_aabb_from_aabb_matrix(this, get_world_transform_mat());

	bounding_volume world{};
	glm::mat4 mat{1.0f};
	mat = glm::translate(mat, m_world.m_position);


	world.m_min = mat * glm::vec4(m_bv.m_min, 1.0f);
	world.m_min = mat * glm::vec4(m_bv.m_max, 1.0f);
	return world;
}

/*!
 * \brief updates new prent therfore new local
 */
void object::update_parent(object* new_parent) {
	//sanity check not to update new parent to the same one 
	if (new_parent == m_parent)
		return;
	//if we are seting our parent to self 
	if (new_parent == this)
		return;
	//tell parent he is no longer our parent
	if (m_parent != nullptr) {
		auto found_self = std::find(m_parent->m_children.begin(), m_parent->m_children.end(), this);
		if (found_self == m_parent->m_children.end()) {
			std::cout << "Something that shouldn't happen just happened. object.cpp [108~]" << std::endl;
		}
		m_parent->m_children.erase(found_self);
	}

	//if we are making this object the root
	if (new_parent == nullptr) {
		//set the local as the world and seet parent as null 
		m_local = m_world;
		m_parent = nullptr;
		return;
	}

	//add child to the list of new parent 
	new_parent->m_children.push_back(this);

	//delete obj if was root before and not any more 
	if (m_parent == nullptr) {
		std::vector<object*>& scene_objs = g_gfx_man()->get_scene()->m_objects;
		auto found_self = std::find(scene_objs.begin(), scene_objs.end(), this);
		if (found_self == scene_objs.end()) {
			std::cout << "Something that shouldn't happen just happened. object.cpp [129~]" << std::endl;
		}
		scene_objs.erase(found_self);
	}


	//get the new local  from this world and new_parent world
	transform new_local{};
	new_local.m_position   = glm::inverse(new_parent->m_world.get_transform_mat()) * glm::vec4{ m_world.m_position, 1.0f };
	new_local.m_scale      = m_world.m_scale / new_parent->m_world.m_scale;
	new_local.m_quaternion = glm::normalize(m_world.m_quaternion * glm::inverse(new_parent->m_world.m_quaternion));

	m_local = new_local;
	m_parent = new_parent;
}

/*!
 * \brief detaches itself from parent and childs too
 */
void object::detach() {
	if (m_parent != nullptr) {
		auto found_self = std::find(m_parent->m_children.begin(), m_parent->m_children.end(), this);
		if (found_self != m_parent->m_children.end()) {
			m_parent->m_children.erase(found_self);
		}
	}
	for (object* obj : m_children)
		obj->detach();
	m_children.clear();
}

/*!
 * \updates the local transform from a new world transform
 */
void object::update_local_from_world() {
	//check if we have parent to calculate from
	if (m_parent == nullptr)
		m_local = m_world;
	else {
		//we calculate the new local transform from the new world and parent world
		m_local.m_position = glm::inverse(m_parent->get_world_transform_mat()) * glm::vec4{ m_world.m_position, 1.0f };
		m_local.m_scale = m_world.m_scale / m_parent->m_world.m_scale;
		m_local.m_quaternion = glm::normalize(glm::inverse(m_parent->m_world.m_quaternion) * m_world.m_quaternion); 
	}
}
