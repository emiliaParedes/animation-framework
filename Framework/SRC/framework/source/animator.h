#pragma once

#include <unordered_map>

class object;
class animation;
class skin;

struct Animator
{
    // animator is always added at the root of the gltf asset node object. 
    // it's job is to link the child nodes with their animation in the 
    // animation data. It's possible for a node not to be animated. 
    // we should account for that when storing the nodes. 
    // store all the nodes of the hierarchy based on their ids. 


    object* asset = nullptr;                                    // gltf asset, contains scene graph
    animation* animData = nullptr;                              // anim data 
    skin* currentSkin = nullptr;                                // current skin
    std::unordered_map<object*, unsigned> skeletonNodes;        // skin nodes (used to draw the skeleton)


    // playback
    float currentTime = 0.0f;
    bool playing = false;
    bool loop = true;


    // functions
    void set_skin(skin* curr_skin);
    void get_skin();
    void initialize();
    void update();
    void render_skeleton();
    bool render_ui();
};


void Animator::Update()
{
    if (animData == NULL)return;
    // produce animation based on current time
    if (playing) {

        // playback management
        currentTime += (f32)aexTime->GetFrameTime();
        f32 duration = animData->times.back();
        if (loop && currentTime > duration)
            currentTime -= duration;

        // for each channel, sample
        u32 i = 0;
        f32 tmpData[16]; // max size is 16 for MAT4
        for (auto channel : animData->channels) {


            // get the node game object and target
            Node& go = asset->Nodes[channel.node];


            // Sample channel
            // depending on the type, we're going to 
            switch (channel.target)
            {
            case Channel::TRANSLATION:  channel.SampleVec3(currentTime, go.local.position, animData);
            case Channel::SCALE:        channel.SampleVec3(currentTime, go.local.scale, animData);
            case Channel::ROTATION:     channel.SampleQuaternion(currentTime, go.local.rotation, animData);
            }
            // increment counter
            ++i;
        }
    }
    DrawSkeleton();
}