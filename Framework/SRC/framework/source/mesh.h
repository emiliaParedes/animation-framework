/*!
 * \file  mesh.h
 * \brief This is a file that contains declaration of class mesh
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <glad/glad.h>
#include "geometry.h"

class shader;
class material;
class texture;

class mesh {
	public:
		std::vector<vertex> m_vertex;
		std::vector<unsigned> m_indices;
		std::vector<unsigned> m_textures;
		material* m_material;
		texture* m_texture;
		GLuint m_vao;
		GLuint m_vbo;
		//GLuint m_vna;	//normals
		//GLuint m_vtc;	//tex coord
		GLuint m_ebo;
		unsigned m_shader;
		unsigned m_faces;
		bool m_transparent;
		mesh();
		~mesh();
		void bind();
		void unbind();
		void create_context();
		void upload();
		void render(const glm::mat4 mat, const glm::mat4 view, const glm::mat4 proj);
		void render_lines(const glm::mat4 mat, const glm::mat4 view, const glm::mat4 proj, glm::vec3 color);
};
