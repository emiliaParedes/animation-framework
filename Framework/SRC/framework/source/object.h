/*!
 * \file  object.h
 * \brief This is a file that contains declaration of class object
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */
#pragma once

#include <glm/glm.hpp>
#include <vector>
#include "transform.h"
#include <string>
#include "geometry.h"

class mesh;

class object {
	private:
		bounding_volume m_bv{};
		bounding_volume m_bv_world{};
		unsigned m_id;
	public:
		object(unsigned id, std::vector<std::string>& mesh_name, transform trans);
		~object();
		object(const std::string& name) :m_name{ name }, m_local{}, m_world{}, m_id{}, m_bv{} {};
		void initialize();
		void render(const glm::mat4& view, const glm::mat4& proj);
		void update(const float& dt);
		void calculate_world();
		glm::mat4 get_world_transform_mat() const;
		glm::mat4 get_local_transform_mat() const;
		bounding_volume get_bv() const { return m_bv_world; }
		void update_local(const transform& new_local);
		bounding_volume get_world_bv() const;
		void update_parent(object* new_parent);
		void detach();
		void update_local_from_world();

		std::vector<object*> m_children;
		std::vector<mesh*> m_meshes;
		object* m_parent = nullptr;
		transform m_local;
		transform m_world;
		std::string m_name;
};