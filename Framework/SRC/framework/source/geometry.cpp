/*!
 * \file  geometry.cpp
 * \brief This is a file that contains definition of functions for geometry
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */
#include "geometry.h"
#include <algorithm>
#include "mesh.h"
#include "object.h"
 
 /*!
  * \brief checks interseciotn with ray and abb
  */
float intersection_ray_aabb(const ray& r, const bounding_volume& obj) {	
	//calculate plane half space
	float near = c_min_float;
	float far = c_max_float;

	//just check if rays origin is inside
	if (intersection_point_aabb(r.m_origin, obj))
		return 0.0f;

	//loop through the 3 planes
	for (unsigned i = 0; i < 3; i++) {
		//check if they are not paralel 
		float inv;
		if (r.m_direction[i] == 0.0f)
			inv = c_max_float;
		else
			inv = 1.0f / r.m_direction[i];
		//get the min and max values 
		float t_min = (obj.m_min[i] - r.m_origin[i]) * inv;
		float t_max = (obj.m_max[i] - r.m_origin[i]) * inv;

		//swap if necesarly 
		if (inv < 0.0f)
			std::swap(t_min, t_max);

		//get the min value 
		near = t_min > near ? t_min : near;

		//get max value
		far = t_max < far ? t_max : far;

		if (far < near)
			return -1.0f;
	}

	//near isnt the near
	if (near < 0.0f) {
		//no intersection
		if (far < 0.0f)
			return -1.0f;
		//far is the nearest one
		return far;
	}

	return near;
}

/*!
 * \brief checks interseciotn with point and abb
 */
bool intersection_point_aabb(const glm::vec3& p, const bounding_volume& obj) {
	//check all coordinates against min and max points of aabb
	bool result = true;

	for (unsigned i = 0; i < 3; i++) {
		//min x, y and z
		if (p[i] < obj.m_min[i])
			result = false;
		//max x, y and z
		if (p[i] > obj.m_max[i])
			result = false;
	}
	return result;
}

/*!
 * \brief gets bv from an aabb and a matrix
 */
bounding_volume bv_aabb_from_aabb_matrix(const object* obj, const glm::mat4& transform) {
	//get the basic aabb from mesh
	bounding_volume basic = bv_aabb_from_obj(obj);
	//transform basic object 

	//get new 8 points
	glm::vec3 size_bv = basic.m_max - basic.m_min;
	glm::vec3 pos = basic.m_min + (size_bv * 0.5f);
	glm::vec3 scale = size_bv;
	std::vector<glm::vec3> new_points{ basic.m_min, basic.m_max};
	new_points.push_back(glm::vec3(basic.m_min + glm::vec3(0.0f, 0.0f, scale.z)));
	new_points.push_back(glm::vec3(basic.m_min + glm::vec3(0.0f, scale.y, 0.0f)));
	new_points.push_back(glm::vec3(basic.m_min + glm::vec3(scale.x, 0.0f, 0.0f)));
	new_points.push_back(glm::vec3(basic.m_max - glm::vec3(0.0f, 0.0f, scale.z)));
	new_points.push_back(glm::vec3(basic.m_max - glm::vec3(0.0f, scale.y, 0.0f)));
	new_points.push_back(glm::vec3(basic.m_max - glm::vec3(scale.x, 0.0f, 0.0f)));
	//transform the aabb
	for (auto& point : new_points) {
		point = transform * glm::vec4(point, 1.0f);
	}
	return bv_aabb_from_points(new_points);
}

/*!
 * \brief gets bv from points
 */
bounding_volume bv_aabb_from_points(const std::vector<glm::vec3>& points) {
	//set the minimun to the maximun possible value
	glm::vec3 min_point{ c_max_float };
	//set maximum value to minimum possible value 
	glm::vec3 max_point{ c_min_float };
	//direction 
	//loop through all the points in mesh 
	unsigned size = static_cast<unsigned>(points.size());
	for (unsigned i = 0; i < size; i++) {
		//min max x
		min_point.x = points[i].x < min_point.x ? points[i].x : min_point.x;
		max_point.x = points[i].x > max_point.x ? points[i].x : max_point.x;
		//min max y
		min_point.y = points[i].y < min_point.y ? points[i].y : min_point.y;
		max_point.y = points[i].y > max_point.y ? points[i].y : max_point.y;
		//min max z
		min_point.z = points[i].z < min_point.z ? points[i].z : min_point.z;
		max_point.z = points[i].z > max_point.z ? points[i].z : max_point.z;
	}
	//create actual aabb
	return bounding_volume(min_point, max_point);

}

/*!
 * \brief gets bv from object
 */
bounding_volume bv_aabb_from_obj(const object* obj) {
	//sanity check 
	if (obj->m_meshes.empty())
		return {};
	//set the minimun to the maximun possible value
	glm::vec3 min_point{ std::numeric_limits<float>::max() };
	//set maximum value to minimum possible value 
	glm::vec3 max_point{ -std::numeric_limits<float>::max() };
	//direction 
	//loop through all the points in mesh 
	for (mesh* c_mesh : obj->m_meshes) {
		unsigned size = static_cast<unsigned>(c_mesh->m_vertex.size());
		for (unsigned i = 0; i < size; i++) {
			glm::vec3 pos = c_mesh->m_vertex[i].m_position;
			//min max x
			min_point.x = pos.x < min_point.x ? pos.x : min_point.x;
			max_point.x = pos.x > max_point.x ? pos.x : max_point.x;
			//min max y
			min_point.y = pos.y < min_point.y ? pos.y : min_point.y;
			max_point.y = pos.y > max_point.y ? pos.y : max_point.y;
			//min max z
			min_point.z = pos.z < min_point.z ? pos.z : min_point.z;
			max_point.z = pos.z > max_point.z ? pos.z : max_point.z;
		}
	}
	//create actual aabb
	return bounding_volume(min_point, max_point);
}

/*!
 * \brief ay intersection
 */
void get_intersection(const ray& r, object* other, float& min, object*& out_obj) {
	bounding_volume obj = other->get_world_bv();
	float dist = intersection_ray_aabb(r, obj);
	if (dist > 0.0f) {
		if (dist < min) {
			min = dist;
			out_obj = other;
		}
	}
	for (object* child : other->m_children) {
		get_intersection(r, child, min, out_obj);
	}
}
