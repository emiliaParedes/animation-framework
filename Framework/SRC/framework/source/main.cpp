/*!
 * \file  main.cpp
 * \brief This is a file that contains main project 
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "graphics_manager.h"	//graphics manager

 /*!
  * \brief main function that contains all calls to other helper functions
  * \param[in] argc (int) number of arguments passed when running the program.
  * \param[in] argc (char *) the strings of the parameters given.
  * \return declares if the program finished correctly or not.
  */
int main(int argc, char** argv) {

	graphics_manager* gfx = graphics_manager::get_gfx_man();

	gfx->initialize();

	while (!gfx->closed()) {
		gfx->clear();
		gfx->update();
		gfx->render();
	}

	return 0;

}