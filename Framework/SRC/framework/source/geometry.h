/*!
 * \file  geometry.h
 * \brief This is a file that contains declaration of class vertex, ray, vounding_volume and geometry functions
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once

#include <glm/glm.hpp>
#include <vector>
#include "../dependencies/src/imgui/imgui.h"

class object;

const float c_epsilon = 0.005f;
const float c_min_float = -std::numeric_limits<float>::max();
const float c_max_float = std::numeric_limits<float>::max();

const glm::vec4 c_locked = glm::vec4(0.5f, 0.1f, 0.9f, 0.15f);
const glm::vec4 c_hovered = glm::vec4(0.92f, 0.54f, 0.2f, 0.1f);

struct vertex {
	glm::vec3 m_position;
	glm::vec2 m_texure_uv;
	glm::vec4 m_color{ .5f };
	glm::vec3 m_normal{ .0f };
	glm::vec3 m_tangent{ .0f };
	glm::vec3 m_bitangent{ .0f };
};

struct ray {
	ray(const glm::vec3& origin, const glm::vec3& dir) : m_origin{ origin }, m_direction{ dir } {}
	glm::vec3 m_origin{ 0.0f };
	glm::vec3 m_direction{ 0.0f };
};

struct bounding_volume {
	bounding_volume() = default;
	bounding_volume(const glm::vec3& min, const glm::vec3& max) : m_min{ min }, m_max{ max }{}
	glm::vec3 m_min{ 0.0f };
	glm::vec3 m_max{ 0.0f };
};

float intersection_ray_aabb(const ray& r, const bounding_volume& obj);
bool intersection_point_aabb(const glm::vec3& p, const bounding_volume& obj);

bounding_volume bv_aabb_from_aabb_matrix(const object* obj, const glm::mat4& transform);
bounding_volume bv_aabb_from_points(const std::vector<glm::vec3>& points);
bounding_volume bv_aabb_from_obj(const object* obj);
void get_intersection(const ray& r, object* other, float& min, object*& out_obj);