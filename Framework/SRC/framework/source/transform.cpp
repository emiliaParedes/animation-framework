/*!
 * \file  transform.cpp
 * \brief This is a file that contains definition of functions for class transform
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "transform.h"
#include <glm/gtx/transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>

 /*!
  * \brief gets only transformation matrix
  */
glm::mat4 transform::get_transform_mat() const {
	glm::mat4 trans{ 1.0f };
	trans = glm::translate(m_position) * 
			glm::toMat4(m_quaternion) *
			glm::scale(m_scale);
	return trans;
}
