/*!
 * \file  camera.h
 * \brief This is a file that contains declaration of class camera
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once
#include <glm/glm.hpp>
#include "transform.h"

class camera {
	enum e_camera_mode {e_free, e_pan, e_orbital};
	public: 
		camera();
		~camera();
		void update(float);
		void rotate(glm::vec2 mouse_delta);
		void move(int key, int mode);
		glm::mat4 get_view_mat() const;
		glm::mat4 get_proj_mat() const;
		void reset();
		
		bool m_rotate;
		glm::vec2 m_last_pos{0.0f};

		glm::vec3 m_position{0.0f};
		glm::vec3 m_rotation{0.0f};

		glm::vec3 m_forward;
		glm::vec3 m_up;
		float m_fov;
		float m_perspective;
		float m_near;
		float m_far;
		float m_speed = 500.0f;
		e_camera_mode m_mode = e_free;
		float m_focal = 100.0f;
};