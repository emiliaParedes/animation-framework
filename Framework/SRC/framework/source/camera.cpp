/*!
 * \file  camera.cpp
 * \brief This is a file that contains definition of functions for class camera
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "camera.h"
#include "graphics_manager.h"

#include <iostream>

#include <glm/ext/matrix_transform.hpp>
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/gtx/transform.hpp>
#include <GLFW/glfw3.h>		//window and input	

 /*!
  * \constructor, sets up all initial information about a camera
  */
camera::camera()
	: m_up{0.0f, 1.0f, 0.0f},
	m_fov{glm::radians(70.0f)},
	m_near{ 0.01f },
	m_far{3000.0f},
	m_rotate{ false },
	m_forward{0.0f, 0.0f, -1.0f },
	m_rotation{ 0.0f, -90.0f, 0.0f }
{
	m_position = glm::vec3(0.0f, -30.0f, 150.0f);
	m_perspective = 1024.0f / 720.0f;
}

/*!
 * \brief destructor
 */
camera::~camera() {
}

/*!
 * \brief nothing
 */
void camera::update(float dt) {
}

/*!
 * \brief rotate camera depending on mode
 */
void camera::rotate(glm::vec2 mouse_delta) {

	if (m_mode == e_pan && m_rotate) {
		const glm::vec3 right = glm::normalize(glm::cross(m_up, m_forward));
		const glm::vec3 delta_mov = right * mouse_delta.x + m_up * mouse_delta.y;
		m_position -= g_gfx_man()->get_delta_time() * m_speed * delta_mov;
		return;
	}

	if (m_mode == e_orbital) {
		glm::vec3 new_center = m_position + m_forward * m_focal;

		float theta{ 0.0f }, gamma{ 0.0f };

		mouse_delta.x /= g_gfx_man()->get_width();
		mouse_delta.y /= g_gfx_man()->get_height();

		gamma = glm::atan(m_position.y / m_position.x);
		theta = glm::acos(m_position.z / m_focal);

		gamma += mouse_delta.x * 0.01f;
		theta += mouse_delta.y * 0.01f;

		m_forward = glm::normalize(-m_position);

		m_position.x += m_focal * glm::sin(theta) * glm::cos(gamma);
		m_position.y += m_focal * glm::sin(theta) * glm::sin(gamma);
		m_position.z += m_focal * glm::cos(theta);

		std::cout << m_position.x << ", " << m_position.y << ", " << m_position.z << std::endl;

		return;

		//angleXZ += increment;
		m_position.z = m_focal * glm::cos(mouse_delta.x);
		m_position.x = m_focal * glm::sin(mouse_delta.x);

		//moving in sin y
		m_position.y += glm::sin(mouse_delta.y);
		return;
	}

	// Vectors of camera movement and speed
	glm::vec3 view = m_forward;
	glm::vec3 up = glm::vec3(0, 1, 0);
	glm::vec3 right = normalize(cross(view, up));
	glm::vec3 position = m_position;
	float current_speed = m_speed;

	// rotation
	if (m_rotate) {		
		m_rotation.x += mouse_delta.y;
		m_rotation.y += mouse_delta.x;

		if (m_rotation.x > 89.0f)
			m_rotation.x = 89.0f;
		if (m_rotation.x < -89.0f)
			m_rotation.x = -89.0f;

		//get new vectors
		glm::vec3 direction;
		direction.x = cos(glm::radians(m_rotation.y)) * cos(glm::radians(m_rotation.x));
		direction.y = sin(glm::radians(m_rotation.x));
		direction.z = sin(glm::radians(m_rotation.y)) * cos(glm::radians(m_rotation.x));
		m_forward = glm::normalize(direction);
		right = glm::normalize(glm::cross(up, m_forward));
		m_up = glm::normalize(glm::cross(m_forward, right));
	}
}

/*!
 * \brief move camera depending on mode
 */
void camera::move(int key, int mode) {
	
	glm::vec3 up = glm::vec3(0, 1, 0);
	glm::vec3 right = glm::normalize(glm::cross(m_forward, up));
	float delta_time = delta_time();

	if (key == GLFW_KEY_LEFT_CONTROL) {
		if (mode == GLFW_PRESS)
			m_mode = e_pan;
		else if(mode == GLFW_RELEASE)
			m_mode = e_free;
		return;
	}

	if (key == GLFW_KEY_LEFT_ALT) {
		if (mode == GLFW_PRESS) {
			m_mode = e_orbital;
		}
		else if (mode == GLFW_RELEASE)
			m_mode = e_free;
		return;
	}

	if (key == GLFW_KEY_UP && (mode == GLFW_PRESS || mode == GLFW_REPEAT)) {
		m_rotation.x += m_speed * g_gfx_man()->get_delta_time();
		glm::vec3 direction;
		direction.x = cos(glm::radians(m_rotation.y)) * cos(glm::radians(m_rotation.x));
		direction.y = sin(glm::radians(m_rotation.x));
		direction.z = sin(glm::radians(m_rotation.y)) * cos(glm::radians(m_rotation.x));
		m_forward = glm::normalize(direction);
		right = glm::normalize(glm::cross(up, m_forward));
		m_up = glm::normalize(glm::cross(m_forward, right));
	}
	if (key == GLFW_KEY_DOWN && (mode == GLFW_PRESS || mode == GLFW_REPEAT)) {
		m_rotation.x -= m_speed * g_gfx_man()->get_delta_time();
		glm::vec3 direction;
		direction.x = cos(glm::radians(m_rotation.y)) * cos(glm::radians(m_rotation.x));
		direction.y = sin(glm::radians(m_rotation.x));
		direction.z = sin(glm::radians(m_rotation.y)) * cos(glm::radians(m_rotation.x));
		m_forward = glm::normalize(direction);
		right = glm::normalize(glm::cross(up, m_forward));
		m_up = glm::normalize(glm::cross(m_forward, right));
	}
	if (key == GLFW_KEY_LEFT && (mode == GLFW_PRESS || mode == GLFW_REPEAT)) {
		m_rotation.y -= m_speed * g_gfx_man()->get_delta_time();
		glm::vec3 direction;
		direction.x = cos(glm::radians(m_rotation.y)) * cos(glm::radians(m_rotation.x));
		direction.y = sin(glm::radians(m_rotation.x));
		direction.z = sin(glm::radians(m_rotation.y)) * cos(glm::radians(m_rotation.x));
		m_forward = glm::normalize(direction);
		right = glm::normalize(glm::cross(up, m_forward));
		m_up = glm::normalize(glm::cross(m_forward, right));
	}
	if (key == GLFW_KEY_RIGHT && (mode == GLFW_PRESS || mode == GLFW_REPEAT)) {
		m_rotation.y += m_speed * g_gfx_man()->get_delta_time();
		glm::vec3 direction;
		direction.x = cos(glm::radians(m_rotation.y)) * cos(glm::radians(m_rotation.x));
		direction.y = sin(glm::radians(m_rotation.x));
		direction.z = sin(glm::radians(m_rotation.y)) * cos(glm::radians(m_rotation.x));
		m_forward = glm::normalize(direction);
		right = glm::normalize(glm::cross(up, m_forward));
		m_up = glm::normalize(glm::cross(m_forward, right));
	}

	if (!m_rotate)
		return;


	if (key == GLFW_KEY_LEFT_SHIFT && (mode == GLFW_PRESS))
		m_speed *= 5.f;
	if (key == GLFW_KEY_LEFT_SHIFT && (mode == GLFW_RELEASE ))
		m_speed /= 5.f;
	if (key == GLFW_KEY_W && (mode == GLFW_PRESS || mode == GLFW_REPEAT))
		m_position += m_forward * m_speed * delta_time;
	if (key ==  GLFW_KEY_S && (mode == GLFW_PRESS || mode == GLFW_REPEAT))
		m_position -= m_forward * m_speed * delta_time;
	if (key == GLFW_KEY_D && (mode == GLFW_PRESS || mode == GLFW_REPEAT))
		m_position += right * m_speed * delta_time;
	if (key ==  GLFW_KEY_A && (mode == GLFW_PRESS || mode == GLFW_REPEAT))
		m_position -= right * m_speed * delta_time;
	if (key == GLFW_KEY_E && (mode == GLFW_PRESS || mode == GLFW_REPEAT))
		m_position += up * m_speed * delta_time;
	if (key == GLFW_KEY_Q && (mode == GLFW_PRESS || mode == GLFW_REPEAT))
		m_position -= up * m_speed * delta_time;


}

/*!
 * \brief view
 */
glm::mat4 camera::get_view_mat() const {
	glm::mat4 view = glm::lookAt(m_position, m_position + m_forward, m_up);
	return view;
}

/*!
 * \briefprojection
 */
glm::mat4 camera::get_proj_mat() const {
	return glm::perspective((m_fov), m_perspective, m_near, m_far);
}

/*!
 * \brief resets values to default
 */
void camera::reset() {
	m_up = glm::vec3(0.0f, 1.0f, 0.0f);
	m_forward = glm::vec3(0.0f, 0.0f, -1.0f);
	m_position = glm::vec3(0.0f, -30.0f, 150.0f);
	m_rotation = glm::vec3{ 0.0f, -90.0f, 0.0f};
	m_rotate = false;
}
