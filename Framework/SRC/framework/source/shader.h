/*!
 * \file  shader.h
 * \brief This is a file that contains declaration of class shader and debug functions
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once

#include <glm/glm.hpp>
#include <iostream>

namespace Debug {
	void  _check_gl_error(const char* file, int line);
}

#define check_gl_error() Debug::_check_gl_error(__FILE__, __LINE__)

class shader {
		unsigned m_vertex;
		unsigned m_fragment;
		unsigned m_program;
	public:
		shader(const std::string& vert, const std::string& frag);
		~shader();
		unsigned get_program() const { return m_program; };

		void set_uniform(const char* varName, int  val);
		void set_uniform(const char* varName, float  val);
		void set_uniform(const char* varName, glm::vec3* val);
		void set_uniform(const char* varName, glm::vec4* val);
		void set_uniform(const char* varName, glm::mat3* val);
		void set_uniform(const char* varName, glm::mat4* val);
};