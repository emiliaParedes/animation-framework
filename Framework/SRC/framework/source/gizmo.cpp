/*!
 * \file  gizmo.cpp
 * \brief This is a file that contains definition of functions for class gizmo
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "gizmo.h"
#include "camera.h"
#include "graphics_manager.h"
#include "geometry.h"
#include "scene.h"
#include "object.h"

#include <iostream>
#include <glm\gtc\type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include "../dependencies/src/imgui/imgui.h"

 /*!
  * \brief constructor
  */
gizmo::gizmo()
	: m_mouse(0.0f),
	m_hover(nullptr),
	m_locked(nullptr),
	m_operation(ImGuizmo::TRANSLATE)
{}

/*!
 * \brief destructor
 */
gizmo::~gizmo() {
}

/*!
 * \brief ImGuizmo init
 */
void gizmo::initialize() {
	ImGuizmo::SetOrthographic(false);
	//true by default
	ImGuizmo::Enable(true);
}

/*!
 * \brief checks the ray on obj to check if choosing another
 */
void gizmo::update(const float& dt) {
	ImGuizmo::BeginFrame();
	//camera position 
	camera* cam = g_gfx_man()->get_camera();
	ray c_ray{cam->m_position, m_mouse};
	//check with objects
	std::vector<object*>& objs = g_gfx_man()->get_scene()->m_objects;
	float min_dist = c_max_float;
	
	object* out = nullptr;
	for (object* obj : objs)
		get_intersection(c_ray, obj, min_dist, out);

	if (out != nullptr) {
		//render hover
		m_hover = out;
	}	
	else {
		m_hover = nullptr;
	}
}

/*!
 * \brief renders the gizmo, hovered and locked bv and if locked, the other window
 */
void gizmo::render(const glm::mat4& view, const glm::mat4 proj) {
	if (m_hover != nullptr && m_hover != m_locked) {
		//render hover color
		g_gfx_man()->render_cube(m_hover->get_world_bv(), c_hovered);
	}
	if (m_locked != nullptr) {
		//render locked and transform 
		g_gfx_man()->render_cube(m_locked->get_world_bv(), c_locked);
		//transform obj
		edit_object(view, proj);
		//imgui transform of obj
		edit_object_ui();
	}
}

/*!
 * \brief locks the hovered object
 */
void gizmo::lock() {
	if (ImGuizmo::IsOver())
		return;
	if (m_hover != nullptr && !ImGuizmo::IsUsing())
		m_locked = m_hover;
	else
		m_locked = nullptr;
}

/*!
 * \brief if there is an item locked
 */
bool gizmo::in_use() {
	if (m_locked != nullptr)
		return true;
	return false;
}

/*!
 * \brief updates the position of the mouse to get the ray check
 */
void gizmo::update_mouse_pos(const double x, const double y) {
	camera* cam = g_gfx_man()->get_camera();
	glm::mat4 inv = cam->get_proj_mat();
	inv = glm::inverse(inv);

	glm::vec4 mouse_pos(x, y, 0.0, 1.0);
	//normalize
	mouse_pos.x = (2.0f * mouse_pos.x) / g_gfx_man()->get_width() - 1.0f;
	mouse_pos.y = 1.0f - (2.0f * mouse_pos.y) / g_gfx_man()->get_height();

	glm::vec4 ray_eye = inv * mouse_pos;
	ray_eye.z = -1.0f;
	ray_eye.w = 0.0f;

	glm::mat4 view = cam->get_view_mat();
	glm::vec3 ray_word = glm::vec3(inverse(view) * ray_eye);
	//normalize 
	ray_word = glm::normalize(ray_word);

	m_mouse = ray_word;
}

/*!
 * \brief sets trans, rot or sca 
 */
void gizmo::set_operation(const int& op) {
	if (in_use()) {
		if (op == 0)
			m_operation = ImGuizmo::TRANSLATE;
		else if (op == 1)
			m_operation = ImGuizmo::SCALE;
		else
			m_operation = ImGuizmo::ROTATE;
	}
}

/*!
 * \brief resets vlaues
 */
void gizmo::reset() {
	m_locked = nullptr;
	m_hover = nullptr;
	m_operation = ImGuizmo::TRANSLATE;
}

/*!
 * \brief edition of object in world 
 */
void gizmo::edit_object(const glm::mat4& view, const glm::mat4& proj) const {
	//send the matrix with the world position and scale of the object with no rotation
	glm::mat4 mat = glm::translate(m_locked->m_world.m_position) * glm::scale(m_locked->m_world.m_scale);

	auto look = glm::value_ptr(view);
	auto pro = glm::value_ptr(proj);
	glm::mat4 id = glm::mat4(1);
	float* identity = glm::value_ptr(id);

	ImGuiIO& io = ImGui::GetIO();
	ImGuizmo::SetRect(1, 1, io.DisplaySize.x, io.DisplaySize.y);

	ImGuizmo::Manipulate(
		look,
		pro,
		m_operation,
		ImGuizmo::WORLD,
		&mat[0][0],
		identity,
		NULL,
		NULL,
		NULL);

	glm::vec3 pos{};
	glm::vec3 rot{};
	glm::vec3 sca{};

	ImGuizmo::DecomposeMatrixToComponents(&mat[0][0], glm::value_ptr(pos), glm::value_ptr(rot), glm::value_ptr(sca));
	
	switch (m_operation) {
		case ImGuizmo::TRANSLATE: {
			//just set it 
			m_locked->m_world.m_position = pos;
			break;
		}
		case ImGuizmo::ROTATE: {
			//get rads instead of deg
			rot = glm::radians(rot);
			//create rotation matrix
			glm::mat4 rot_mat = glm::rotate(rot.x, glm::vec3(1.0f, 0.0f, 0.0f))*
								glm::rotate(rot.y, glm::vec3(0.0f, 1.0f, 0.0f)) *
								glm::rotate(rot.z, glm::vec3(0.0f, 0.0f, 1.0f));
			//get the new quaternion by multiplying the new rotation and the last world quaternion
			m_locked->m_world.m_quaternion = glm::normalize(glm::toQuat(rot_mat) * m_locked->m_world.m_quaternion);
			break;
		}
		case ImGuizmo::SCALE: {
			//just set
			m_locked->m_world.m_scale = sca;
		}
	}
	m_locked->update_local_from_world();
}

/*!
 * \brief renders ui inspector
 */
void gizmo::edit_object_ui() {
	//sanity check 
	if (m_locked == nullptr)
		return ;
	ImGui::Begin("Object Info");

	ImGui::Text("Name: ");
	ImGui::SameLine();
	ImGui::Text(m_locked->m_name.c_str());

	ImGui::NewLine();
	if (ImGui::Button("Delete Object")) {
		delete m_locked;
		m_locked = nullptr;
		return;
	}
	ImGui::NewLine();
	
	ImGui::Text("Parent: ");
	ImGui::SameLine();
	if (m_locked->m_parent == nullptr)
		ImGui::Text("Scene");
	else 
		ImGui::Text(m_locked->m_parent->m_name.c_str());


	object* new_parent = nullptr;
	bool got_new = g_gfx_man()->get_scene()->ui_change_parent(new_parent, m_locked);
	if (got_new) {
		//set obj to redo stuff 
		m_locked->update_parent(new_parent);
		if (new_parent == nullptr)
			g_gfx_man()->get_scene()->m_objects.push_back(m_locked);
	}

	transform local = m_locked->m_local;
	transform world = m_locked->m_world;
	
	ImGui::NewLine();

	ImGui::Text("Local Position");
	if (ImGui::DragFloat3("", &(local.m_position[0])))
		m_locked->update_local(local);

	ImGui::Text("Local Scale");
	if(ImGui::DragFloat3(" ", &(local.m_scale[0])))
		m_locked->update_local(local);

	ImGui::Text("Local Rotation");
	if(ImGui::DragFloat4("  ", &(local.m_quaternion[0])))
		m_locked->update_local(local);

	ImGui::NewLine();

	ImGui::Text("World Position. Read Only");
	ImGui::DragFloat3("   ", &(world.m_position[0]));

	ImGui::Text("World Scale");
	ImGui::DragFloat3("    ", &(world.m_scale[0]));

	ImGui::Text("World Rotation");
	ImGui::DragFloat4("     ", &(world.m_quaternion[0]));
	ImGui::End();
}
