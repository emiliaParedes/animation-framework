/*!
 * \file  scene.hs
 * \brief This is a file that contains declaration of class scene
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once
#include <vector>
#include <string>
class object;
class light;
class resource_manager;
class camera;
class importer;
namespace objl {
	class Loader;
}

class scene {
	public:
		scene();
		~scene();
		//void load_scene(const std::string& path);
		void load_scene(const std::vector<std::string>& paths);
		void load_scene(const std::string& path);
		void unload_scene();
		void update(const float& dt);
		void add_light();
		void ui_render() const;
		void ui_node(const object* node) const;
		void ui_render_nodes_rec(object* node, const object* curr_obj);
		bool ui_change_parent(object*& out_obj, const object* curr_obj);
		camera* m_camera;
		std::vector<object*> m_objects;
		std::vector<light*> m_lights;
		importer* m_importer;
		object* m_parent_change = nullptr;
};