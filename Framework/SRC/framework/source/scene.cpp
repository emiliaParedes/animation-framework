/*!
 * \file  scene.cpp
 * \brief This is a file that contains definition of functions for class scene
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "scene.h"

//libs
#include <assimp/cimport.h>
#include <assimp/postprocess.h>	//not sure about this one 
#include <assimp/scene.h>
#include <vector>
#include <glm/gtc/random.hpp>

//imgui 
#include "..\dependencies\src\imgui\imgui.h"
#include "..\dependencies\src\imgui\imgui_impl_glfw.h"
#include "..\dependencies\src\imgui\imgui_impl_opengl3.h"

//graphics
#include "shader.h"
#include "resources.h"
#include "mesh.h"
#include "graphics_manager.h"
#include "scene_loader.h"
#include "camera.h"
#include "importer.h"
#include "object.h"
#include "light.h"
#include "gizmo.h"

 /*!
  * \brief constructor
  */
scene::scene()
	: m_camera{ nullptr },
	m_objects{}, 
	m_lights{},
	m_importer{nullptr}
{
	scene* curr_scene = g_gfx_man()->get_scene();
	if (curr_scene != nullptr)
		curr_scene->unload_scene();
	g_gfx_man()->set_scene(this);
	if (m_camera == nullptr)
		m_camera = new camera();

	if (m_importer == nullptr)
		m_importer = new importer{};

	add_light();

}
/*!
 * \brief destructor
 */
scene::~scene() {
	unload_scene();
	if (m_importer != nullptr)
		delete m_importer;
	m_importer = nullptr;
	if (m_camera != nullptr)
		delete m_camera;
	m_camera = nullptr;
}

/*void scene::load_scene(const std::string& path) {
	

	std::vector<std::string> scenes{};
	//scenes.push_back("assets/sponza/Sponza.gltf");
	//scenes.push_back("assets/box/Box.gltf");
	scenes.push_back(path);
	//scenes.push_back("data/meshes/sponza.obj");
	//scenes.push_back("data/meshes/suzanne.obj");
	m_objects.push_back(m_importer->import_file(scenes));

	for (object* obj : m_objects)
		obj->initialize();
	//scene_loader(path, m_camera, m_objects);

}*/

/*!
 * \brief loads the scenes from file paths
 */
void scene::load_scene(const std::vector<std::string>& paths) {
	m_objects.push_back(m_importer->import_file(paths));

	for (object* obj : m_objects)
		obj->initialize();
	
}

/*!
 * \brief same
 */
void scene::load_scene(const std::string& path) {
	m_objects.push_back(m_importer->import_file({ path }));
	m_objects.back()->initialize();
}

/*!
 * \brief unload all assets and objects from scene
 */
void scene::unload_scene() {
	for (object* obj : m_objects)
		delete obj;
	m_objects.clear();
	//for (light* lig : m_lights)
	//	delete lig;
	//m_lights.clear();
	m_parent_change = nullptr;
}

/*!
  * \brief updates the scene and objects
  */
void scene::update(const float& dt) {
	for (object* node : m_objects)
		node->update(dt);
}

/*!
 * \brief adds a reg light
 */
void scene::add_light() {

	light* new_light = new light();
	//new_light->m_color = glm::sphericalRand(1.0f);
	//new_light->m_transform.m_position = glm::vec3(0.0f);// glm::sphericalRand(30.0f);// +glm::vec3(0.0f, 0.0f, 0.0f);
	m_lights.push_back(new_light);
}

/*!
 * \brief renders the ui of the scene
 */
void scene::ui_render() const {

	if (ImGui::CollapsingHeader("Lights")){
		/*ALL LIGHTS HERE */
		ImGui::Text("Position");
		ImGui::DragFloat3("", &m_lights[0]->m_transform.m_position[0]);
		ImGui::Text("Color");
		ImGui::DragFloat3(" ", &m_lights[0]->m_color[0]);
		ImGui::Text("Intensity");
		ImGui::DragFloat("  ", &m_lights[0]->m_constant, 0.001f, 0.0f, 1.0f);
	}
	ImGui::NewLine();

	if (ImGui::CollapsingHeader("Objects:")) {
		for (object* obj : m_objects)
			ui_node(obj);
	}
}

/*!
 * \brief recursive for ui
 */
void scene::ui_node(const object* node) const {
	if (ImGui::TreeNode(node->m_name.c_str())) {
		if (ImGui::IsMouseClicked(1)){
			g_gfx_man()->get_gizmo()->lock(const_cast<object*>(node));
		}
		for (object* child: node->m_children)
			ui_node(child);
		ImGui::TreePop();
	}
}

/*!
 * \brief recursive for ui
 */
void scene::ui_render_nodes_rec(object* node, const object* curr_obj) {
	if (curr_obj == node)
		return;
	if (ImGui::Selectable(node->m_name.c_str(), m_parent_change == node))
		m_parent_change = node;
	for(object* obj : node->m_children)
		ui_render_nodes_rec(obj, curr_obj);
}

/*!
 * \brief changes parent for object given 
 */
bool scene::ui_change_parent(object*& out_obj, const object* curr_obj) {
	m_parent_change = nullptr;
	if (!ImGui::CollapsingHeader("Change Parent")) {
		out_obj = m_parent_change;
		return false;
	}
	if (ImGui::Selectable("Set As Root", false)) {
		m_parent_change = nullptr;
		return true;
	}
	for (object* obj : m_objects) {
		ui_render_nodes_rec(obj, curr_obj);
	}
	out_obj = m_parent_change;
	if (m_parent_change == nullptr)
		return false;
	return true;
}
