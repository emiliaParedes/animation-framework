/*!
 * \file  resources.cpp
 * \brief This is a file that contains definition of functions for class resources
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "resources.h"

//resources 
#include "mesh.h"
#include "shader.h"

#define STB_IMAGE_IMPLEMENTATION
#include "..\dependencies\src\stbi\stb_image.h"

#include <iostream>

#include <assimp/cimport.h>
#include <assimp/postprocess.h>	//not sure about this one 
#include <assimp/scene.h>

 /*!
  * \brief constructor
  */
texture::texture(const std::string& path) {
	int width{}, height{}, channels{};
	unsigned char* m_info;
	m_info = stbi_load(path.c_str(), &width, &height, &channels, 4);

	GLenum glformat;
	switch (channels) {
		case 1: glformat = GL_RED;  break;
		case 2: glformat = GL_RG;   break;
		case 3: glformat = GL_RGB;  break;
		case 4: glformat = GL_RGBA; break;
	}
	glformat = GL_RGBA;
	if (m_info == nullptr) {
		std::cout << "Failed to load texture: " << path << std::endl;
		return;
	}

	// Create texture
	glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
	glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
	glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
	//glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &m_id);
	glBindTexture(GL_TEXTURE_2D, m_id);
	check_gl_error();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	check_gl_error();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	check_gl_error();
	// Give pixel data to opengl
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexImage2D(GL_TEXTURE_2D, 0, glformat, width, height, 0, glformat, GL_UNSIGNED_BYTE, m_info);
	check_gl_error();
	stbi_image_free(m_info);
}

/*!
 * \brief destructor
 */
texture::~texture(){
	glDeleteTextures(1, &m_id);
}

resource_manager* resource_manager::m_resource_man = nullptr;

/*!
 * \brief constructor
 */
resource_manager::resource_manager()
	: m_meshes{},
	m_shaders{},
	m_textures{} {
	stbi_set_flip_vertically_on_load(true);
}

/*!
 * \brief destructor
 */
resource_manager::~resource_manager() {
	unload();
}

/*!
 * \brief loads scenes from the given paths
 */
void resource_manager::load_scenes(const std::vector<std::string>& scenes) {
	for (const std::string& name : scenes) {
		const aiScene* scene = aiImportFile(name.c_str(), 
			aiProcess_CalcTangentSpace |
			aiProcess_Triangulate |
			aiProcess_JoinIdenticalVertices |
			aiProcess_SortByPType);

		if (scene == nullptr) {
			std::cout << name << " was not loaded." << std::endl;
			return;
		}

		if (scene->HasTextures())
			std::cout << "HELL YEAHHH" << std::endl;

		std::string base = name;
		size_t pos = name.find_last_of('/');
		base = base.substr(0, pos + 1);
		//material 
		for (unsigned i = 0; i < scene->mNumMaterials; i++) {
			aiMaterial* mat = scene->mMaterials[i];
			aiColor3D color{};
			aiColor4D mtlDiffuse{};
			if (AI_SUCCESS != mat->Get(AI_MATKEY_COLOR_DIFFUSE, color))
				std::cout << "lol1" << std::endl;
			m_materials.push_back(new material());
			m_materials.back()->m_diffuse = glm::vec3(color.r, color.g, color.b);
			if (AI_SUCCESS != mat->Get(AI_MATKEY_COLOR_SPECULAR, color))
				std::cout << "lol2" << std::endl;
			m_materials.back()->m_specular = glm::vec3(color.r, color.g, color.b);
			if (AI_SUCCESS != mat->Get(AI_MATKEY_COLOR_AMBIENT, color))
				std::cout << "lol3" << std::endl;
			m_materials.back()->m_ambient = glm::vec3(color.r, color.g, color.b);
			float ex{};
			if (AI_SUCCESS != mat->Get(AI_MATKEY_SHININESS, m_materials.back()->m_shininess))
				std::cout << "lol4" << std::endl;
			m_materials.back()->m_shininess = ex;
			mat->Get(AI_MATKEY_SHININESS_STRENGTH, ex);
				//std::cout << ex << std::endl;
			m_materials.back()->m_emission = ex;
			aiString name; 
			if (AI_SUCCESS != mat->Get(AI_MATKEY_NAME, name))
				std::cout << "lol6" << std::endl;
			std::cout <<"	 " << name.data << std::endl;
			//for () {
			//
			//}
			

			aiString texturePath{};
			for (int k = 1; k < aiTextureType_BASE_COLOR; k++) {
				unsigned numTextures = mat->GetTextureCount(static_cast<aiTextureType>(aiTextureType_NONE + k));
				for (unsigned y = 0; y < numTextures; y++) {
					mat->GetTexture(static_cast<aiTextureType>(aiTextureType_NONE + k), y, &texturePath);
					if (m_textures.find(texturePath.data) == m_textures.end())
						m_textures.insert(std::make_pair(texturePath.data, new texture(base + texturePath.data)));
					m_materials.back()->m_textures.insert(std::make_pair(static_cast<e_texture_type>(k), m_textures.at(texturePath.data)));
					std::cout << texturePath.data << std::endl;
				}
			}
		}

		//loop through each mesh and store the information necesarly
		for (size_t i = 0; i < scene->mNumMeshes; i++) {
			//curr mesh & our new mesh
			const aiMesh* curr_mesh = scene->mMeshes[i];
			mesh* new_mesh = new mesh{};
			new_mesh->m_shader = static_cast<int>(m_shaders.size() - 1);
			new_mesh->m_material = m_materials[curr_mesh->mMaterialIndex];

			//indices
			new_mesh->m_indices.reserve(curr_mesh->mNumFaces * 3);
			for (size_t j = 0; j < curr_mesh->mNumFaces; j++) {
				//current face
				const aiFace& face = curr_mesh->mFaces[j];
				for (int k = 0; k < 3; k++)
					new_mesh->m_indices.push_back(*(face.mIndices + k));
			}
			new_mesh->m_faces = curr_mesh->mNumFaces;


			//vertex init 
			new_mesh->m_vertex.reserve(curr_mesh->mNumVertices);


			//vertices
			//new_mesh->m_positions.reserve(curr_mesh->mNumVertices);
			if (curr_mesh->HasPositions()) {
				for (size_t j = 0; j < curr_mesh->mNumVertices; j++) {
					const aiVector3D* vec = curr_mesh->mVertices + j;
					//new_mesh->m_positions.push_back(glm::vec3(vec->x, vec->y, vec->z));
					new_mesh->m_vertex.push_back({});
					new_mesh->m_vertex.back().m_position = glm::vec3(vec->x, vec->y, vec->z);
					const unsigned colors = curr_mesh->GetNumColorChannels();
					if (colors > 0) {
						const aiColor4D* color = curr_mesh->mColors[0];
						new_mesh->m_vertex.back().m_color = glm::vec4(color->r, color->g, color->b, color->a);
					}
				}
			}

			//normals
			//new_mesh->m_normals.reserve(curr_mesh->mNumVertices);
			if (curr_mesh->HasNormals()) {
				for (size_t j = 0; j < curr_mesh->mNumVertices; j++) {
					const aiVector3D* vec = curr_mesh->mNormals + j;
					//new_mesh->m_normals.push_back(glm::vec3(vec->x, vec->y, vec->z));
					new_mesh->m_vertex[j].m_normal = glm::vec3(vec->x, vec->y, vec->z);
				}
			}

			//texture coords 
			//new_mesh->m_tex_coord.reserve(curr_mesh->mNumVertices * 2);
			if (curr_mesh->HasTextureCoords(0)) {
				for (size_t j = 0; j < curr_mesh->mNumVertices; j++) {
					const aiVector3D& vec = curr_mesh->mTextureCoords[0][j];
					//new_mesh->m_tex_coord.push_back(glm::vec2(vec->x, vec->y));
					new_mesh->m_vertex[j].m_texure_uv = glm::vec2(vec.x, vec.y);
				}
			}

			new_mesh->m_material = m_materials[curr_mesh->mMaterialIndex];
			aiString texturePath{};
			scene->mMaterials[curr_mesh->mMaterialIndex]->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath);
			new_mesh->m_texture = m_textures[texturePath.data];
			new_mesh->upload();
			m_meshes.insert(std::make_pair(std::string(curr_mesh->mName.data), new_mesh));
		}
	}
}

/*!
 * \brief loades the needed shaders
 */
void resource_manager::load_shaders() {
	m_shaders.push_back(new shader("resources/shaders/basic_shader.vertex", "resources/shaders/basic_shader.frag"));
	m_shaders.push_back(new shader("resources/shaders/lighting_shader.vertex", "resources/shaders/lighting_shader.frag"));
}

/*!
 * \brief reloads all shaders
 */
void resource_manager::reload_shaders() {
	for (shader* shader_ : m_shaders)
		delete shader_;
	m_shaders.clear();
	load_shaders();
}

/*!
 * \brief default values 
 */
void resource_manager::initialize() {
	//shaders 
	load_shaders();

	//basic model
	mesh* cube = new mesh();
	for (unsigned i = 0; i < 36; i++)
		cube->m_vertex.push_back({});

	cube->m_vertex[0].m_position = { -0.5f, -0.5f, -0.5f };
	cube->m_vertex[1].m_position = { +0.5f, -0.5f, -0.5f };
	cube->m_vertex[2].m_position = { +0.5f, +0.5f, -0.5f };
	cube->m_vertex[3].m_position = { +0.5f, +0.5f, -0.5f };
	cube->m_vertex[4].m_position = { -0.5f, +0.5f, -0.5f };
	cube->m_vertex[5].m_position = { -0.5f, -0.5f, -0.5f };

	cube->m_vertex[6].m_position = { -0.5f, -0.5f, +0.5f };
	cube->m_vertex[7].m_position = { +0.5f, -0.5f, +0.5f };
	cube->m_vertex[8].m_position = { +0.5f, +0.5f, +0.5f };
	cube->m_vertex[9].m_position = { +0.5f, +0.5f, +0.5f };
	cube->m_vertex[10].m_position = { -0.5f, +0.5f, +0.5f };
	cube->m_vertex[11].m_position = { -0.5f, -0.5f, +0.5f };

	cube->m_vertex[12].m_position = { -0.5f, +0.5f, +0.5f };
	cube->m_vertex[13].m_position = { -0.5f, +0.5f, -0.5f };
	cube->m_vertex[14].m_position = { -0.5f, -0.5f, -0.5f };
	cube->m_vertex[15].m_position = { -0.5f, -0.5f, -0.5f };
	cube->m_vertex[16].m_position = { -0.5f, -0.5f, +0.5f };
	cube->m_vertex[17].m_position = { -0.5f, +0.5f, +0.5f };

	cube->m_vertex[18].m_position = { +0.5f, +0.5f, +0.5f };
	cube->m_vertex[19].m_position = { +0.5f, +0.5f, -0.5f };
	cube->m_vertex[20].m_position = { +0.5f, -0.5f, -0.5f };
	cube->m_vertex[21].m_position = { +0.5f, -0.5f, -0.5f };
	cube->m_vertex[22].m_position = { +0.5f, -0.5f, +0.5f };
	cube->m_vertex[23].m_position = { +0.5f, +0.5f, +0.5f };

	cube->m_vertex[24].m_position = { -0.5f, -0.5f, -0.5f };
	cube->m_vertex[25].m_position = { +0.5f, -0.5f, -0.5f };
	cube->m_vertex[26].m_position = { +0.5f, -0.5f, +0.5f };
	cube->m_vertex[27].m_position = { +0.5f, -0.5f, +0.5f };
	cube->m_vertex[28].m_position = { -0.5f, -0.5f, +0.5f };
	cube->m_vertex[29].m_position = { -0.5f, -0.5f, -0.5f };

	cube->m_vertex[30].m_position = { -0.5f, +0.5f, -0.5f };
	cube->m_vertex[31].m_position = { +0.5f, +0.5f, -0.5f };
	cube->m_vertex[32].m_position = { +0.5f, +0.5f, +0.5f };
	cube->m_vertex[33].m_position = { +0.5f, +0.5f, +0.5f };
	cube->m_vertex[34].m_position = { -0.5f, +0.5f, +0.5f };
	cube->m_vertex[35].m_position = { -0.5f, +0.5f, -0.5f };

	//indices
	for (unsigned i = 0; i < 36; i++)
		cube->m_indices.push_back(i);
	cube->upload();
	m_meshes.insert(std::make_pair("bv", cube));

	//std::vector<shader*> m_shaders = resource->get_shaders();
	//std::vector<material*> m_materials = resource->get_materials();
	//std::map<std::string, texture*> m_textures = resource->get_textures();
	//std::vector<mesh*> m_meshes = resource->get_meshes();
	//const aiScene* scene = aiImportFile("assets/scenes/meshes/sponza.obj", aiProcessPreset_TargetRealtime_Fast);

	//load_scenes(scenes);
	//load scene with assimp
	
	//m_meshes.push_back(new mesh());
	//m_meshes.back()->m_vertex.push_back({});
	//m_meshes.back()->m_vertex.back().m_position = glm::vec3{ -0.5f, 0.5f, 0.0f};
	//m_meshes.back()->m_vertex.push_back({});
	//m_meshes.back()->m_vertex.back().m_position = glm::vec3{ -0.5f, -0.5f, 0.0f };
	//m_meshes.back()->m_vertex.push_back({});
	//m_meshes.back()->m_vertex.back().m_position = glm::vec3{ 0.5f,  -0.5f, 0.0f };
	//for (unsigned i = 0; i < 3; i++)
	//	m_meshes.back()->m_indices.push_back(i);
	//
	//m_meshes.back()->upload();
	//m_meshes.back()->m_shader = m_shaders.back();
}

/*!
 * \brief reloads entire manager
 */
void resource_manager::reload() {
	unload();
	initialize();
}

/*!
 * \brief downloads all info in manager
 */
void resource_manager::unload() {
	for (const std::pair<std::string, mesh*>& mesh_ : m_meshes)
		delete mesh_.second;
	m_meshes.clear();
	for (shader* shader_ : m_shaders)
		delete shader_;
	m_shaders.clear();
	for (const std::pair<std::string, texture*>& texture_ : m_textures)
		delete texture_.second;
	m_textures.clear();
	for (material* mat : m_materials)
		delete mat;
	m_materials.clear();
}
