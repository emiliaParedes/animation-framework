#pragma once

#include "channel.h"

#include <vector>
#include <string>

struct animation {
    std::vector<channel> channels;
    std::vector<channel> channels;
    float GetDuration();    
    bool IsNodePresent(std::string nodeName);
}