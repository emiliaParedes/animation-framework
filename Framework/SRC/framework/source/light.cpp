/*!
 * \file  light.cpp
 * \brief This is a file that contains definition of functions for class light
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "light.h"
#include <glm/gtc/random.hpp>
#include <glm/trigonometric.hpp>

 /*!
  * \brief constructor
  */
light::light()
	: m_transform{},
	m_color{ 1.0f },
	m_constant{ 1.0f },
	m_linear{ 0.09f },
	m_quadratic{ 0.032f }
{ 
	m_transform.m_position = glm::vec3(150.0f, 150.0f, 150.0f);
}

