/*!
 * \file  resources.h
 * \brief This is a file that contains declaration of class texture, enum texture type, class material and class resource
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#pragma once

#include <string>
#include <vector>
#include <map>
#include <glad/glad.h>
#include <glm/glm.hpp>

class texture {
	public:
		GLuint m_id{};
		texture(const std::string& path);
		~texture();
};

enum e_texture_type {
	tex_type_diffuse = 1,
	tex_type_specular,
	tex_type_ambient,
	tex_type_emissive,
	tex_type_height,
	tex_type_normal,
	tex_type_shininess,
	tex_type_opacity,
	tex_type_displacement,
	tex_type_lightmap,
	tex_type_refrection,
	tex_type_base_color, 
	tex_type_unknown
};

class material {
	public:
		material() = default;
		glm::vec3 m_diffuse{};
		glm::vec3 m_ambient{};
		glm::vec3 m_specular{};
		float m_emission{};
		float m_shininess{};
		std::map<e_texture_type, texture*> m_textures;
};

class shader;
class mesh;

class resource_manager {
	private:
		std::map<std::string, texture*> m_textures;
		std::vector<shader*> m_shaders;
		std::vector<material*> m_materials;
		static resource_manager* m_resource_man;
		resource_manager();
		~resource_manager();
		void load_scenes(const std::vector<std::string>& scenes);
		void load_shaders();
	public:
		std::map<std::string, mesh*> m_meshes;
		//singleton implementation 
		inline static resource_manager* get_resource_man() { if (m_resource_man == nullptr) m_resource_man = new resource_manager(); return m_resource_man; };
		void reload_shaders();
		void initialize();
		void reload();
		void unload();
		std::map<std::string, texture*>& get_textures() { return m_textures; };
		std::vector<shader*>& get_shaders() { return m_shaders; }
		std::map<std::string,mesh*>& get_meshes() { return m_meshes; }
		std::vector<material*>& get_materials() {	return m_materials;}
};

#define res_man() resource_manager::get_resource_man()

