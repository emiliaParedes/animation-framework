/*!
 * \file  importer.cpp
 * \brief This is a file that contains definition of functions for class importer
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */

#include "importer.h"

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include "resources.h"
#include "mesh.h"
#include "object.h"

 /*!
  * \brief gets a color from material
  */
bool importer::get_material_parameter(aiMaterial* material, const char* type, int, int, aiColor3D* out) {
	if (AI_SUCCESS != material->Get(AI_MATKEY_COLOR_DIFFUSE, out)) {
		//std::cout << "Material doesn't have " << type << " parameter." << std::endl;
		return false;
	}
	return true;
}

/*!
 * \brief gets a float from material
 */
bool importer::get_material_parameter(aiMaterial* material, const char* type, int, int, float* out) {
	if (AI_SUCCESS != material->Get(AI_MATKEY_COLOR_DIFFUSE, out)) {
		//std::cout << "Material doesn't have " << type << " parameter." << std::endl;
		return false;
	}
	return true;
}

/*!
 * \brief gets textures from materials
 */
void importer::get_material_textures(aiMaterial* material, const std::string& path_base) {
	aiString texturePath{};
	auto m_textures = res_man()->get_textures();
	auto m_materials = res_man()->get_materials();

	for (int k = 1; k < aiTextureType_BASE_COLOR; k++) {
		unsigned int numTextures = material->GetTextureCount(static_cast<aiTextureType>(aiTextureType_NONE + k));
		for (unsigned y = 0; y < numTextures; y++) {
			material->GetTexture(static_cast<aiTextureType>(aiTextureType_NONE + k), y, &texturePath);
			std::string texture_name{ texturePath.data };
			size_t pos = texture_name.find_first_of('/') + 1;
			texture_name = texture_name.substr(pos);

			std::string texture_path{texturePath.data};
			pos = texture_path.find_last_of('/') + 1;
			texture_path = texture_path.substr(pos);
			if (m_textures.find(texture_path) == m_textures.end())
				m_textures.insert(std::make_pair(texture_path, new texture(path_base + texture_name)));
			m_materials.back()->m_textures.insert(std::make_pair(static_cast<e_texture_type>(k + y), m_textures.at(texture_path)));
		}
	}
}

/*!
 * \brief gets the mesh from gltf
 */
mesh* importer::process_mesh(aiMesh* curr_mesh, const aiScene* curr_scene) {
	auto& m_materials = res_man()->get_materials();
	auto& m_meshes = res_man()->get_meshes();
	auto& m_textures = res_man()->get_textures();

	mesh* new_mesh = new mesh{};
	new_mesh->m_shader = 0;				//base 
	new_mesh->m_material = m_materials[curr_mesh->mMaterialIndex];

	//indices
	new_mesh->m_indices.reserve(curr_mesh->mNumFaces * 3);
	for (size_t j = 0; j < curr_mesh->mNumFaces; j++) {
		//current face
		const aiFace& face = curr_mesh->mFaces[j];
		for (int k = 0; k < 3; k++)
			new_mesh->m_indices.push_back(*(face.mIndices + k));
	}
	new_mesh->m_faces = curr_mesh->mNumFaces;


	//vertex init 
	new_mesh->m_vertex.reserve(curr_mesh->mNumVertices);


	//vertices
	//new_mesh->m_positions.reserve(curr_mesh->mNumVertices);
	if (curr_mesh->HasPositions()) {
		for (size_t j = 0; j < curr_mesh->mNumVertices; j++) {
			const aiVector3D* vec = curr_mesh->mVertices + j;
			//new_mesh->m_positions.push_back(glm::vec3(vec->x, vec->y, vec->z));
			new_mesh->m_vertex.push_back({});
			new_mesh->m_vertex.back().m_position = glm::vec3(vec->x, vec->y, vec->z);
			const unsigned colors = curr_mesh->GetNumColorChannels();
			if (colors > 0) {
				const aiColor4D* color = curr_mesh->mColors[0];
				new_mesh->m_vertex.back().m_color = glm::vec4(color->r, color->g, color->b, color->a);
			}
		}
	}

	//normals
	//new_mesh->m_normals.reserve(curr_mesh->mNumVertices);
	if (curr_mesh->HasNormals()) {
		for (size_t j = 0; j < curr_mesh->mNumVertices; j++) {
			const aiVector3D* vec = curr_mesh->mNormals + j;
			//new_mesh->m_normals.push_back(glm::vec3(vec->x, vec->y, vec->z));
			new_mesh->m_vertex[j].m_normal = glm::vec3(vec->x, vec->y, vec->z);
		}
	}

	//texture coords 
	//new_mesh->m_tex_coord.reserve(curr_mesh->mNumVertices * 2);
	if (curr_mesh->HasTextureCoords(0)) {
		for (size_t j = 0; j < curr_mesh->mNumVertices; j++) {
			const aiVector3D& vec = curr_mesh->mTextureCoords[0][j];
			//new_mesh->m_tex_coord.push_back(glm::vec2(vec->x, vec->y));
			new_mesh->m_vertex[j].m_texure_uv = glm::vec2(vec.x, vec.y);
		}
	}

	new_mesh->m_material = m_materials[curr_mesh->mMaterialIndex];
	aiString texturePath{};
	curr_scene->mMaterials[curr_mesh->mMaterialIndex]->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath);
	new_mesh->m_texture = m_textures[texturePath.data];
	new_mesh->upload();
	m_meshes.insert(std::make_pair(std::string(curr_mesh->mName.data), new_mesh));
	return new_mesh;
}

/*!
 * \brief process the object then to mesh
 */
object* importer::process_node(aiNode* curr_node, const aiScene* curr_scene) {
	//create obj to it 
	object* new_object = new object(curr_node->mName.data);
	aiVector3D pos{}, scale{};
	aiQuaternion rot;
	curr_node->mTransformation.Decompose(scale, rot, pos);
	new_object->m_local.m_position = glm::vec3(pos.x, pos.y, pos.z);
	new_object->m_local.m_scale = glm::vec3(scale.x, scale.y, scale.z);
	new_object->m_local.m_quaternion.w = rot.w;
	new_object->m_local.m_quaternion.x = rot.x;
	new_object->m_local.m_quaternion.y = rot.y;
	new_object->m_local.m_quaternion.z = rot.z;
	//get all meshes in node
	for (unsigned i = 0; i < curr_node->mNumMeshes; i++)
		new_object->m_meshes.push_back(process_mesh(curr_scene->mMeshes[curr_node->mMeshes[i]], curr_scene));
	//get all children nodes
	for (unsigned i = 0; i < curr_node->mNumChildren; i++) {
		object* child = process_node(curr_node->mChildren[i], curr_scene);
		child->m_parent = new_object;
		new_object->m_children.push_back(child);
	}
	return new_object;
}

/*!
 * \brief basic function to call
 */
object* importer::import_file(std::vector<std::string> scenes) {
	auto& m_materials = res_man()->get_materials();
	auto& m_meshes = res_man()->get_meshes();
	auto& m_textures = res_man()->get_textures();

	for (const std::string& name : scenes) {
		const aiScene* scene = aiImportFile(name.c_str(),
			aiProcess_CalcTangentSpace |
			aiProcess_Triangulate |
			aiProcess_JoinIdenticalVertices |
			aiProcess_SortByPType);

		if (scene == nullptr) {
			std::cout << name << " was not loaded." << std::endl;
			return nullptr;
		}

		if (scene->HasTextures())
			std::cout << "HELL YEAHHH" << std::endl;

		std::string base = name;
		size_t pos = name.find_last_of('/');
		base = base.substr(0, pos + 1);
		//material 
		for (unsigned i = 0; i < scene->mNumMaterials; i++) {
			aiMaterial* mat = scene->mMaterials[i];
			m_materials.push_back(new material());

			aiColor3D color{};
			mat->Get(AI_MATKEY_COLOR_DIFFUSE, color);
			m_materials.back()->m_diffuse = glm::vec3(color.r, color.g, color.b);

			mat->Get(AI_MATKEY_COLOR_SPECULAR, color);
			m_materials.back()->m_specular = glm::vec3(color.r, color.g, color.b);

			mat->Get(AI_MATKEY_COLOR_AMBIENT, color);
			m_materials.back()->m_ambient = glm::vec3(color.r, color.g, color.b);

			float ex{};
			mat->Get(AI_MATKEY_SHININESS, ex);
			m_materials.back()->m_shininess = ex;

			mat->Get(AI_MATKEY_SHININESS_STRENGTH, ex);
			m_materials.back()->m_emission = ex;
			
			aiString name;
			mat->Get(AI_MATKEY_NAME, name);

			get_material_textures(mat, base);
		}

		return process_node(scene->mRootNode, scene);
	}
}
