/*!
 * \file  shader.cpp
 * \brief This is a file that contains definition of functions for class shader
 * \author Maria Emilia Paredes C.
 *         m.paredes@digipen.edu
 *         540001917
 * \date 01-10-2020 (Last Updated)
 */
#include "shader.h"

#include <glad/glad.h>
#include <sstream>
#include <vector>
#include <fstream>

namespace Debug {

	/*!
	 * \brief debug info
	 */
	void  _check_gl_error(const char* file, int line) {

		GLenum err(glGetError());

		while (err != GL_NO_ERROR) {
			std::string error;

			switch (err) {
			case GL_INVALID_OPERATION:      error = "INVALID_OPERATION";      break;
			case GL_INVALID_ENUM:           error = "INVALID_ENUM";           break;
			case GL_INVALID_VALUE:          error = "INVALID_VALUE";          break;
			case GL_OUT_OF_MEMORY:          error = "OUT_OF_MEMORY";          break;
			case GL_INVALID_FRAMEBUFFER_OPERATION:  error = "INVALID_FRAMEBUFFER_OPERATION";  break;
			}

			// format error message
			std::stringstream errorMsg{};
			errorMsg << "GL_" << error.c_str() << " - " << file << ":" << line << std::endl;

			// send to VS outpput
			std::cout << (errorMsg.str().c_str());

			// repeat
			err = glGetError();
		}
	}
}

namespace Helpers {

	/*!
	 * \brief reads the file from shader
	 */
	std::string get_shader(const std::string& path)
	{
		//information from shader
		std::string content;
		//creating file stream
		std::ifstream fileStream(path, std::ios::in);

		//if file not found or couldn't be opened
		if (!fileStream.is_open()) {
			std::cout << "Could not read file " << path << ". File does not exist." << std::endl;
		}

		//start reading
		std::string line = "";
		while (!fileStream.eof()) {
			std::getline(fileStream, line);
			content.append(line + "\n");
		}

		//close stream 
		fileStream.close();
		//return the information read
		return content;
	}
}

/*!
 * \brief loads frag and vertex and licks to program
 */
shader::shader(const std::string& vert, const std::string& frag)
	: m_fragment{},
	m_program{},
	m_vertex{}
{
	//create vertex shader
	m_vertex = glCreateShader(GL_VERTEX_SHADER);
	check_gl_error();
	std::string temp = Helpers::get_shader(vert);
	const char* src_vtx = temp.c_str();
	glShaderSource(m_vertex, 1, &src_vtx, NULL);
	check_gl_error();
	glCompileShader(m_vertex);
	check_gl_error();
	GLint success = 0;
	glGetShaderiv(m_vertex, GL_COMPILE_STATUS, &success);
	if (success == GL_FALSE) {
		std::cout << "dang it" << std::endl;
		GLint logSize = 0;
		glGetShaderiv(m_vertex, GL_INFO_LOG_LENGTH, &logSize);
		std::vector<GLchar> info_log(logSize);
		glGetShaderInfoLog(m_vertex, logSize, &logSize, &info_log[0]);
		std::cout << info_log.data() << std::endl;
	}


	//fragment shader
	m_fragment = glCreateShader(GL_FRAGMENT_SHADER);
	check_gl_error();
	temp = Helpers::get_shader(frag);
	const char* src_frag = temp.c_str();
	glShaderSource(m_fragment, 1, &src_frag, NULL);
	check_gl_error();
	glCompileShader(m_fragment);
	check_gl_error();
	glGetShaderiv(m_fragment, GL_COMPILE_STATUS, &success);
	if (success == GL_FALSE) {
		std::cout << "dang it2" << std::endl;
		GLint logSize = 0;
		glGetShaderiv(m_fragment, GL_INFO_LOG_LENGTH, &logSize);
		std::vector<GLchar> info_log(logSize);
		glGetShaderInfoLog(m_fragment, logSize, &logSize, &info_log[0]);
		std::cout << info_log.data() << std::endl;
	}

	//create program and attach shaders to it
	m_program = glCreateProgram();
	check_gl_error();
	glAttachShader(m_program, m_vertex);
	check_gl_error();
	glAttachShader(m_program, m_fragment);
	check_gl_error();
	glLinkProgram(m_program);
	GLint isLinked = 0;
	check_gl_error();
	glGetProgramiv(m_program, GL_LINK_STATUS, &isLinked);
	check_gl_error();
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<GLchar> info_log(maxLength);
		glGetProgramInfoLog(m_program, maxLength, &maxLength, &info_log[0]);
		std::cout << info_log.data() << std::endl;
		// The program is useless now. So delete it.
		glDeleteProgram(m_program);

		// Provide the infolog in whatever manner you deem best.
		// Exit with failure.
		return;
	}


	check_gl_error();
	//delete info. no needed 
	glDeleteShader(m_vertex);
	glDeleteShader(m_fragment);
}

/*!
 * \brief destructor
 */
shader::~shader() {
	glDeleteShader(m_vertex);
	glDeleteShader(m_fragment);
}

/**
* Sets a uniform in shaders
* @param varName - name of uniform
* @param val - value to set
*/
void shader::set_uniform(const char* varName, int val) {

	GLint loc = glGetUniformLocation(m_program, varName);
	if (loc != -1)
		glUniform1i(loc, val);
	else
		std::cout << "Uniform " << varName << " not found" << std::endl;
	check_gl_error();
}

/**
* Sets a uniform in shaders
* @param varName - name of uniform
* @param val - value to set
*/
void shader::set_uniform(const char* varName, float val) {
	GLint loc = glGetUniformLocation(m_program, varName);
	if (loc != -1)
		glUniform1f(loc, val);
	else
		std::cout << "Uniform " << varName << " not found" << std::endl;
	check_gl_error();
}

/**
* Sets a uniform in shaders
* @param varName - name of uniform
* @param val - value to set
*/
void shader::set_uniform(const char* varName, glm::vec3* val) {

	GLint loc = glGetUniformLocation(m_program, varName);
	if (loc != -1)
		glUniform3fv(loc, 1, reinterpret_cast<GLfloat*>(val));
	else
		std::cout << "Uniform " << varName << " not found" << std::endl;
	check_gl_error();
}

/**
* Sets a uniform in shaders
* @param varName - name of uniform
* @param val - value to set
*/
void shader::set_uniform(const char* varName, glm::vec4* val) {

	GLint loc = glGetUniformLocation(m_program, varName);
	if (loc != -1)
		glUniform4fv(loc, 1, reinterpret_cast<GLfloat*>(val));
	else
		std::cout << "Uniform " << varName << " not found" << std::endl;
	check_gl_error();
}

/**
* Sets a uniform in shaders
* @param varName - name of uniform
* @param val - value to set
*/
void shader::set_uniform(const char* varName, glm::mat3* val) {

	GLint loc = glGetUniformLocation(m_program, varName);
	if (loc != -1)
		glUniformMatrix3fv(loc, 1, GL_FALSE, reinterpret_cast<GLfloat*>(val));
	else
		std::cout << "Uniform " << varName << " not found" << std::endl;
	check_gl_error();
}

/**
* Sets a uniform in shaders
* @param varName - name of uniform
* @param val - value to set
*/
void shader::set_uniform(const char* varName, glm::mat4* val) {

	GLint loc = glGetUniformLocation(m_program, varName);
	check_gl_error();
	if (loc != -1)
		glUniformMatrix4fv(loc, 1, GL_FALSE, reinterpret_cast<GLfloat*>(val));
	else
		std::cout << "Uniform " << varName << " not found" << std::endl;
	check_gl_error();
}