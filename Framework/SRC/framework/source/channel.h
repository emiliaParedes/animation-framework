#pragma once

#include <glm/glm.hpp>

struct channel {
    glm::vec3 SampleVec3(float time);    
    glm::quat SampleQuaternion(float time);
}
